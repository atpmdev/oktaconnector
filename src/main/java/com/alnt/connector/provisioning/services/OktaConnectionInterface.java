package com.alnt.connector.provisioning.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningAttributes;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.access.provisioning.model.IProvisioningStatus;
import com.alnt.access.provisioning.model.IRoleAuditInfo;
import com.alnt.access.provisioning.model.ISystemInformation;
import com.alnt.access.provisioning.model.ProvisioningAttributes;
import com.alnt.access.provisioning.model.SystemInformation;
import com.alnt.access.provisioning.services.IConnectionInterface;
import com.alnt.access.provisioning.utils.ConnectorUtil;
import com.alnt.connector.constants.OktaConnectorConstants;
import com.alnt.connector.exception.OktaConnectorException;
import com.alnt.connector.helper.OktaClientHelper;
import com.alnt.connector.helper.OktaURLConnection;
import com.alnt.connector.helper.Partition;
import com.alnt.connector.provisioning.model.AuthenticationProvider;
import com.alnt.connector.provisioning.model.Group;
import com.alnt.connector.provisioning.model.PasswordCredential;
import com.alnt.connector.provisioning.model.ProvisioningResult;
import com.alnt.connector.provisioning.model.RecoveryQuestionCredential;
import com.alnt.connector.provisioning.model.RoleAuditInfo;
import com.alnt.connector.provisioning.model.RoleInformation;
import com.alnt.connector.provisioning.model.User;
import com.alnt.connector.provisioning.model.UserCredentials;
import com.alnt.connector.provisioning.model.UserInformation;
import com.alnt.connector.provisioning.model.UserStatus;
import com.alnt.extractionconnector.common.constants.IExtractionConstants;
import com.alnt.extractionconnector.common.constants.IExtractionConstants.USER_ENTITLEMENT_KEY;
import com.alnt.extractionconnector.common.model.IUserInformation;
import com.alnt.extractionconnector.common.service.IExtractionInterface;
import com.alnt.extractionconnector.common.service.ISearchCallback;
import com.alnt.extractionconnector.exception.ExtractorConnectionException;
import com.alnt.extractionconnector.user.model.ExtractorAttributes;
import com.alnt.fabric.component.rolemanagement.search.IRoleInformation;
import com.alntgoogle.gson.Gson;
import com.alntgoogle.gson.GsonBuilder;
import com.alntgoogle.gson.reflect.TypeToken;

/**
 * 
 * @author soori
 *
 */
public class OktaConnectionInterface implements IConnectionInterface, IExtractionInterface {

	private static final String CLASS_NAME = "com.alnt.connector.provisioning.services.OktaConnectionInterface";
	private static Logger logger = Logger.getLogger(CLASS_NAME);
	private String _baseUrl = null;
	private String _idToken = null;
	private String _alertAppDateFormat = null;
	private String externalUserIdAttribute = null;
	private boolean _showProvisioningWarnings = true;
	private boolean _activateUserWhileCreate = false;
	protected Set<String> excludeLogAttrList = null;
	protected Set<String> customAttrList = null;

	public OktaConnectionInterface(Map<String, String> connectionParams) throws OktaConnectorException {
		try {
			_baseUrl = (String) connectionParams.get(OktaConnectorConstants.ORG_URL);
			_idToken = (String) connectionParams.get(OktaConnectorConstants.ID_TOKEN);
			_alertAppDateFormat = (String) connectionParams.get(OktaConnectorConstants.ALERT_APP_DATE_FORMAT);
			if (connectionParams.containsKey(OktaConnectorConstants.SHOW_PROVISION_WARNINGS)) {
				_showProvisioningWarnings = Boolean.parseBoolean(connectionParams.get(OktaConnectorConstants.SHOW_PROVISION_WARNINGS));
			}
			if (connectionParams.containsKey(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE)) {
				_activateUserWhileCreate = Boolean.parseBoolean(connectionParams.get(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE));
			}
			externalUserIdAttribute = OktaConnectorConstants.OKTA_USERID;
			String excludeAttributes = connectionParams.get(OktaConnectorConstants.SENSITIVE_ATTRIBUTES);
			if (excludeAttributes != null && !excludeAttributes.isEmpty()) {
				excludeLogAttrList = ConnectorUtil.convertStringToList(excludeAttributes, ",");
			}
			if (excludeLogAttrList == null) {
				excludeLogAttrList = new HashSet<String>();
			}
			String customAttributes = connectionParams.get(OktaConnectorConstants.CUSTOM_ATTRIBUTES);
			if (customAttributes != null && !customAttributes.isEmpty()) {
				customAttrList = ConnectorUtil.convertStringToList(customAttributes, ",");
			}
			if (customAttrList == null) {
				customAttrList = new HashSet<String>();
			}
		} catch (Exception _e) {
			throw new OktaConnectorException("connection parameters should be not be NULL");
		}
	}

	// RECON OPERATIONS :: BEGIN

	@Override
	public void getAllRoles(Map<String, List<ExtractorAttributes>> options, int fetchSize, Map<String, String> searchCriteria, ISearchCallback callback) throws ExtractorConnectionException {
		try {
			logger.debug(CLASS_NAME + " Start of getAllRoles(),  Begin");
			List<IRoleInformation> rolesList = new ArrayList<IRoleInformation>();
			// TODO:: add filters , by default it returns 10000
			List<Group> groups = getAllGroups();
			for (Group group : groups) {
				String roleId = group.getId();
				String roleName = group.getProfile().getName();
				String roleDesc = group.getProfile().getDescription();
				IRoleInformation roleInformation = getRoleInformation(options, roleName, roleDesc);
				Map<String, List<String>> memberData = new HashMap<String, List<String>>();
				List<String> values = new ArrayList<String>();
				values.add("" + roleId);
				memberData.put(IExtractionConstants.TECHNICAL_ROLE_NAME, values);
				getRoleAttribute(options, OktaConnectorConstants.ATTR_ROLE_ID, roleId, memberData);
				getRoleAttribute(options, OktaConnectorConstants.ATTR_ROLE_NAME, roleName, memberData);
				getRoleAttribute(options, OktaConnectorConstants.ATTR_ROLE_DESC, roleDesc, memberData);
				roleInformation.setMemberData(memberData);
				rolesList.add(roleInformation);
			}
			callback.processSearchResult(rolesList);
			logger.debug(CLASS_NAME + " getAllRoles(): End of getAllRoles(), ");
		} catch (Exception e) {
			logger.error(CLASS_NAME + " getRoles() : execution failed ", e);
		}
	}

	@Override
	public void getIncrementalRoles(Date lastRunDate, Map<String, List<ExtractorAttributes>> options, int fetchSize, Map<String, String> searchCriteria, ISearchCallback callback) throws ExtractorConnectionException {
		// TODO:: GET -- {{url}}/api/v1/groups?filter=lastUpdated gt "2015-10-05T00:00:00.000Z" or lastMembershipUpdated gt "2015-10-05T00:00:00.000Z"
		try {

			// ?filter=lastmodified gt datetime'2019-12-12T01:30:00'
			// NOTE :: following filter is not working , it is retuning full records
			String lastRunString = parseDateOktaStringFormat(lastRunDate);
			String filterQuery = "?filter=lastUpdated gt '" + lastRunString + "'  or lastMembershipUpdated gt '" + lastRunString + "'";
			String endPoint = _baseUrl.concat(OktaConnectorConstants.ENDPOINT_GROUPS).concat(filterQuery);
			// String endPoint = _baseUrl.concat(OktaConnectorConstants.ENDPOINT_ACCESS_LEVELS);
			logger.debug(CLASS_NAME + " Start of getIncrementalRoles(), Begin");
			List<IRoleInformation> rolesList = new ArrayList<IRoleInformation>();
			List<Group> groups = new ArrayList<Group>();
			OktaURLConnection rs2Connection = OktaClientHelper.getConnection(endPoint, OktaConnectorConstants.REQ_METHOD_TYPE_GET, _idToken);
			if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					JSONArray restapiresponseArray = new JSONArray(output);
					if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
						Type listType = new TypeToken<List<Group>>() {
						}.getType();
						Gson gsonParser = new GsonBuilder().setDateFormat(OktaConnectorConstants.DEFAULT_DATE_FORMAT_SECONDS).create();
						groups = gsonParser.fromJson(String.valueOf(restapiresponseArray), listType);
					}
				}
			}
			for (Group group : groups) {
				String roleId = group.getId();
				String roleName = group.getProfile().getName();
				String roleDesc = group.getProfile().getDescription();
				IRoleInformation roleInformation = getRoleInformation(options, roleName, roleDesc);
				Map<String, List<String>> memberData = new HashMap<String, List<String>>();
				List<String> values = new ArrayList<String>();
				values.add("" + roleId);
				memberData.put(IExtractionConstants.TECHNICAL_ROLE_NAME, values);
				getRoleAttribute(options, OktaConnectorConstants.ATTR_ROLE_ID, roleId, memberData);
				getRoleAttribute(options, OktaConnectorConstants.ATTR_ROLE_NAME, roleName, memberData);
				roleInformation.setMemberData(memberData);
				rolesList.add(roleInformation);
			}
			callback.processSearchResult(rolesList);
			logger.debug(CLASS_NAME + " getIncrementalRoles(): End  ");
		} catch (Exception e) {
			logger.error(CLASS_NAME + " getIncrementalRoles() : execution failed ", e);
		}

	}

	@Override
	public void getAllUsersWithCallback(Map<String, List<ExtractorAttributes>> options, int fetchSize, Map<String, String> searchCriteria, ISearchCallback callback) throws ExtractorConnectionException {
		// TODO :: GET --{{url}}/api/v1/users?limit=4
		try {
			logger.debug(CLASS_NAME + " getAllUsersWithCallback(): Start of getAllUsersWithCallback method");
			long start = System.currentTimeMillis();
			int totalUsers = 0;
			List<IUserInformation> userInfoList = new ArrayList<IUserInformation>();
			List<User> users = new ArrayList<User>();
			String endPoint = _baseUrl.concat(OktaConnectorConstants.ENDPOINT_GET_USER);
			logger.debug(CLASS_NAME + " getAllUsersWithCallback(): endPoint " + endPoint);
			OktaURLConnection rs2Connection = OktaClientHelper.getConnection(endPoint, OktaConnectorConstants.REQ_METHOD_TYPE_GET, _idToken);
			if (rs2Connection.getResponseCode() != HttpStatus.SC_OK) {
				logger.error(CLASS_NAME + " getAllUsersWithCallback()  Error While fetching Users " + rs2Connection.getResponseMessage());
			} else if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					JSONArray restapiresponseArray = new JSONArray(output);
					if (null != restapiresponseArray && restapiresponseArray.length() > 0) {

						Type listType = new TypeToken<List<User>>() {
						}.getType();
						Gson gsonParser = new GsonBuilder().setDateFormat(OktaConnectorConstants.DEFAULT_DATE_FORMAT_SECONDS).create();
						users = gsonParser.fromJson(String.valueOf(restapiresponseArray), listType);
					}
				}
			}
			logger.debug(CLASS_NAME + " getAllUsersWithCallback(): found  " + users.size());
			totalUsers = totalUsers + users.size();
			if (null != users && !users.isEmpty()) {
				for (User user : users) {
					IUserInformation userInfo = processExternalUserInfo(user, options, null);
					userInfoList.add(userInfo);
				}
				if (fetchSize > 0) {
					List<List<IUserInformation>> data = (Partition.ofSize(userInfoList, fetchSize));
					for (List<IUserInformation> chunk : data) {
						callback.processSearchResult(chunk);
					}
				} else {
					callback.processSearchResult(userInfoList);
				}
			}
			long end = System.currentTimeMillis();
			float msec = end - start;
			float sec = msec / 1000F;
			float minutes = sec / 60F;
			logger.debug(CLASS_NAME + " getAllUsersWithCallback(): total time taken for  " + totalUsers + "  User Recon   is " + minutes + "  minutes");
		} catch (Exception _ex) {
			logger.error(CLASS_NAME + " getAllUsersWithCallback()  Exception While fetching Users " + _ex.getLocalizedMessage());
		}
	}

	@Override
	public void getIncrementalUsersWithCallback(Date lastRunDate, Map<String, List<ExtractorAttributes>> options, final int fetchSize, Map<String, String> searchCriteria, ISearchCallback callback) throws ExtractorConnectionException {
		// TODO:: {{url}}/api/v1/users?filter=status eq "ACTIVE" and lastUpdated gt "2014-02-01T00:00:00.000Z"&limit=5
		try {
			String lastRunString = parseDateOktaStringFormat(lastRunDate);
			logger.debug(CLASS_NAME + " getIncrementalUsersWithCallback(): Start of getIncrementalUsersWithCallback method");
			long start = System.currentTimeMillis();
			int totalUsers = 0;
			List<IUserInformation> userInfoList = new ArrayList<IUserInformation>();
			List<User> users = new ArrayList<User>();
			String filterQuery = "?filter=lastUpdated gt '" + lastRunString + "'";
			String endPoint = _baseUrl.concat(OktaConnectorConstants.ENDPOINT_GET_USER).concat(filterQuery);
			logger.debug(CLASS_NAME + " getIncrementalUsersWithCallback(): endPoint " + endPoint);
			OktaURLConnection rs2Connection = OktaClientHelper.getConnection(endPoint, OktaConnectorConstants.REQ_METHOD_TYPE_GET, _idToken);
			if (rs2Connection.getResponseCode() != HttpStatus.SC_OK) {
				logger.error(CLASS_NAME + " getIncrementalUsersWithCallback()  Error While fetching Users " + rs2Connection.getResponseMessage());
			} else if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					JSONArray restapiresponseArray = new JSONArray(output);
					if (null != restapiresponseArray && restapiresponseArray.length() > 0) {

						Type listType = new TypeToken<List<User>>() {
						}.getType();
						Gson gsonParser = new GsonBuilder().setDateFormat(OktaConnectorConstants.DEFAULT_DATE_FORMAT_SECONDS).create();
						users = gsonParser.fromJson(String.valueOf(restapiresponseArray), listType);
					}
				}
			}
			logger.debug(CLASS_NAME + " getIncrementalUsersWithCallback(): found  " + users.size());
			totalUsers = totalUsers + users.size();
			if (null != users && !users.isEmpty()) {
				for (User user : users) {
					IUserInformation userInfo = processExternalUserInfo(user, options, null);
					userInfoList.add(userInfo);
				}
				if (fetchSize > 0) {
					List<List<IUserInformation>> data = (Partition.ofSize(userInfoList, fetchSize));
					for (List<IUserInformation> chunk : data) {
						callback.processSearchResult(chunk);
					}
				} else {
					callback.processSearchResult(userInfoList);
				}
			}
			long end = System.currentTimeMillis();
			float msec = end - start;
			float sec = msec / 1000F;
			float minutes = sec / 60F;
			logger.debug(CLASS_NAME + " getIncrementalUsersWithCallback(): total time taken for  " + totalUsers + "  User Recon   is " + minutes + "  minutes");
		} catch (Exception _ex) {
			logger.error(CLASS_NAME + " getIncrementalUsersWithCallback()  Exception While fetching Users " + _ex.getLocalizedMessage());
		}
	}

	private IUserInformation processExternalUserInfo(User user, Map<String, List<ExtractorAttributes>> options, String lastRunAt) {
		logger.debug(CLASS_NAME + " processExternalUserInfo(): Begin  :: ");
		IUserInformation userInformation = new UserInformation();
		Map<String, Map<String, List<Map<String, Object>>>> allEntitlements = new HashMap<String, Map<String, List<Map<String, Object>>>>();
		Map<String, Object> userAttr = new HashMap<String, Object>();
		if (null != user) {
			Map<String, String> profile = user.getProfile();
			userAttr = convertFormat(OktaConnectorConstants.OKTA_USERID, user.getId(), options, userAttr, "User");
			if (null != profile && !profile.isEmpty()) {
				for (String sytemDefinedOptinalAttribute : OktaConnectorConstants.SYSTEM_DEFINED_ATTRIBTESS) {
					if (profile.get(sytemDefinedOptinalAttribute) != null && !profile.get(sytemDefinedOptinalAttribute).toString().isEmpty()) {
						userAttr = convertFormat(sytemDefinedOptinalAttribute, profile.get(sytemDefinedOptinalAttribute).toString(), options, userAttr, "User");
					}
				}
				for (String customAttribute : customAttrList) {
					if (profile.get(customAttribute) != null && !profile.get(customAttribute).toString().isEmpty()) {
						userAttr = convertFormat(customAttribute, profile.get(customAttribute).toString(), options, userAttr, "User");
					}
				}
			}
			List<Map<String, Object>> rolesList = new ArrayList<Map<String, Object>>();
			List<Group> groups = getUserGroups(user.getId());
			if (null != groups && !groups.isEmpty()) {
				for (Group userRole : groups) {
					logger.debug(CLASS_NAME + " processExternalUserInfo(): start processing user role information");
					Map<String, Object> processedDataMap = new HashMap<String, Object>();
					processedDataMap = convertFormat(OktaConnectorConstants.ATTR_ROLE_ID, userRole.getId(), options, processedDataMap, "Role");
					processedDataMap = convertFormat(OktaConnectorConstants.ATTR_ROLE_NAME, userRole.getProfile().getName(), options, processedDataMap, "Role");
					processedDataMap = convertFormat(OktaConnectorConstants.ATTR_ROLE_DESC, userRole.getProfile().getDescription(), options, processedDataMap, "Role");
					rolesList.add(processedDataMap);
					logger.debug(CLASS_NAME + " processExternalUserInfo(): finished user role information processing");
				}
			}
			if (rolesList != null && rolesList.size() > 0) {
				Map<String, List<Map<String, Object>>> entitlement = new HashMap<String, List<Map<String, Object>>>();
				entitlement.put(USER_ENTITLEMENT_KEY.ROLES.toString(), rolesList);
				allEntitlements.put(user.getId(), entitlement);
				/*
				 * if (userAttr.containsKey(OktaConnectorConstants.AE_USERID) && userAttr.get(OktaConnectorConstants.AE_USERID) != null) { allEntitlements.put(userAttr.get(OktaConnectorConstants.AE_USERID).toString(), entitlement); } else {
				 * logger.debug("Alert attribute userId is not mapped to any of OktaConnectorConstants external system's attribute or mapped external attriute is null"); }
				 */
			}
			userInformation.setEntitlements(allEntitlements);
			userInformation.setUserDetails(userAttr);
		}
		return userInformation;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:28:18 pm
	 * 
	 * @param fieldName
	 * @param fieldValue
	 * @param options
	 * @param userAttr
	 * @param type
	 * @return
	 *
	 */
	private Map<String, Object> convertFormat(String fieldName, Object fieldValue, Map<String, List<ExtractorAttributes>> options, Map<String, Object> userAttr, String type) {

		try {
			if (options != null && options.size() > 0 && options.containsKey(fieldName)) {
				List<ExtractorAttributes> extractorAttributesList = options.get(fieldName);
				if (extractorAttributesList != null && extractorAttributesList.size() > 0) {
					for (ExtractorAttributes extractorAttribute : extractorAttributesList) {
						boolean typeMatch = (type.equals("User") && extractorAttribute.isUserAttr()) || (type.equals("Badge") && extractorAttribute.isBadgeAttr()) || (type.equals("Role") && extractorAttribute.isRoleAttr());
						if (extractorAttribute != null && typeMatch) {
							logger.debug(CLASS_NAME + " processOutMap() : setting value for: " + extractorAttribute.getAttributeName());
							userAttr.put(extractorAttribute.getAttributeName(), fieldValue != null ? String.valueOf(fieldValue) : null);
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " convertFormat() : Error setting the value for " + fieldName, e);
		}
		return userAttr;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:34:22 pm
	 * 
	 * @param options
	 * @param roleId
	 * @param roleDesc
	 * @return IRoleInformation
	 *
	 */
	private IRoleInformation getRoleInformation(Map<String, List<ExtractorAttributes>> options, String roleId, String roleDesc) {
		IRoleInformation roleInformation = new RoleInformation();
		logger.trace(CLASS_NAME + " getRoleInformation(): start processing role name " + roleDesc);
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(roleDesc);
		// Adepu's Changes
		List<ExtractorAttributes> aeRoleNameAttr = options.get(OktaConnectorConstants.ATTR_ROLE_ID);
		if (aeRoleNameAttr != null) {
			for (ExtractorAttributes extractorAttributes : aeRoleNameAttr) {
				if (extractorAttributes.isRoleAttr() && OktaConnectorConstants.ALERT_ROLE_NAME.equals(extractorAttributes.getAttributeName())) {
					roleInformation.setName(roleId);
				}
			}
		}
		roleInformation.setDescription(roleDesc);
		roleInformation.setValidFrom(null);
		roleInformation.setValidTo(null);
		logger.trace(CLASS_NAME + " getRoleInformation(): finish processing role name " + roleDesc);
		return roleInformation;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:06:34 pm void
	 * 
	 * @param options
	 * @param attributeName
	 * @param attributeValue
	 * @param memberData
	 *
	 */
	private void getRoleAttribute(Map<String, List<ExtractorAttributes>> options, String attributeName, String attributeValue, Map<String, List<String>> memberData) {
		if (options.containsKey(attributeName)) {
			List<String> values = new ArrayList<String>();
			List<ExtractorAttributes> companyAttr = options.get(attributeName);
			for (ExtractorAttributes extractorAttributes : companyAttr) {
				if (extractorAttributes.isRoleAttr()) {
					values = new ArrayList<String>();
					values.add("" + attributeValue);
					memberData.put(extractorAttributes.getAttributeName(), values);
				}
			}
		}
	}

	@Override
	public List<IUserInformation> getIncrementalUsers(Date arg0, Map<String, List<ExtractorAttributes>> arg1, int arg2, int arg3, Map<String, String> arg4) throws ExtractorConnectionException {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Map getUsers(Map arg0, List arg1) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public void getUsers(String arg0, ISearchCallback arg1) throws ExtractorConnectionException {
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void invokeUsers(Map arg0, List arg1) throws ExtractorConnectionException {
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getAllRoles(String arg0) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public List<com.alnt.extractionconnector.common.model.IRoleInformation> getAllRoles(Map<String, List<ExtractorAttributes>> arg0, int arg1, int arg2, Map<String, String> arg3) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public List<com.alnt.extractionconnector.common.model.IRoleInformation> getIncrementalRoles(Date arg0, Map<String, List<ExtractorAttributes>> arg1, int arg2, int arg3, Map<String, String> arg4) throws ExtractorConnectionException {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getRoles(String arg0) throws ExtractorConnectionException {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getRolesForUser(String arg0) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public void searchRoles(String arg0, ISearchCallback arg1) throws ExtractorConnectionException {
	}

	@Override
	public boolean supportsProvisioning() {
		return false;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Map getAllUsers(Map arg0, List arg1) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public List<IUserInformation> getAllUsers(Map<String, List<ExtractorAttributes>> arg0, int arg1, int arg2, Map<String, String> arg3) throws ExtractorConnectionException {
		return null;
	}
	// RECON OPERATIONS :: END

	// PROVISIONING OPERATIONS :: BEGIN
	/**
	 * 
	 * soori 2019-12-02 - 5:36:27 pm
	 * 
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#testConnection()
	 *
	 *
	 */
	@Override
	public boolean testConnection() throws Exception {
		logger.info(CLASS_NAME + " testConnection(): Start of testConnection method");
		try {
			String endPoint = _baseUrl.concat(OktaConnectorConstants.ENDPOINT_TEST);
			OktaURLConnection oktaConnection = OktaClientHelper.getConnection(endPoint, OktaConnectorConstants.REQ_METHOD_TYPE_GET, _idToken);
			if (oktaConnection.getResponseCode() != 200) {
				logger.debug("Test connection failed , " + oktaConnection.getResponseMessage());
				return false;
			}
			return true;
		} catch (Exception _ex) {
			logger.debug("Test connection failed , " + _ex.getLocalizedMessage());
			return false;
		}
	}

	// TODO
	@Override
	public List<IProvisioningAttributes> getAttributes() throws Exception {
		List<IProvisioningAttributes> provAttributes = new ArrayList<IProvisioningAttributes>();
		// Mandatory Attributes
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, true, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, true, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_SECONDEMAIL, OktaConnectorConstants.ATTR_USER_PROFILE_SECONDEMAIL, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, true, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, true, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_MIDDLENAME, OktaConnectorConstants.ATTR_USER_PROFILE_MIDDLENAME, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_HONORIFICPREFIX, OktaConnectorConstants.ATTR_USER_PROFILE_HONORIFICPREFIX, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_HONORIFICSUFFIX, OktaConnectorConstants.ATTR_USER_PROFILE_HONORIFICSUFFIX, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_TITLE, OktaConnectorConstants.ATTR_USER_PROFILE_TITLE, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_DISPLAYNAME, OktaConnectorConstants.ATTR_USER_PROFILE_DISPLAYNAME, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_NICKNAME, OktaConnectorConstants.ATTR_USER_PROFILE_NICKNAME, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_PROFILEURL, OktaConnectorConstants.ATTR_USER_PROFILE_PROFILEURL, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_PRIMARYPHONE, OktaConnectorConstants.ATTR_USER_PROFILE_PRIMARYPHONE, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_MOBILEPHONE, OktaConnectorConstants.ATTR_USER_PROFILE_MOBILEPHONE, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_STREETADDRESS, OktaConnectorConstants.ATTR_USER_PROFILE_STREETADDRESS, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_CITY, OktaConnectorConstants.ATTR_USER_PROFILE_CITY, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_STATE, OktaConnectorConstants.ATTR_USER_PROFILE_STATE, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_ZIPCODE, OktaConnectorConstants.ATTR_USER_PROFILE_ZIPCODE, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_COUNTRYCODE, OktaConnectorConstants.ATTR_USER_PROFILE_COUNTRYCODE, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_POSTALADDRESS, OktaConnectorConstants.ATTR_USER_PROFILE_POSTALADDRESS, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_PREFERREDLANGUAGE, OktaConnectorConstants.ATTR_USER_PROFILE_PREFERREDLANGUAGE, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_LOCALE, OktaConnectorConstants.ATTR_USER_PROFILE_LOCALE, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_TIMEZONE, OktaConnectorConstants.ATTR_USER_PROFILE_TIMEZONE, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_USERTYPE, OktaConnectorConstants.ATTR_USER_PROFILE_USERTYPE, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_EMPLOYEENUMBER, OktaConnectorConstants.ATTR_USER_PROFILE_EMPLOYEENUMBER, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_COSTCENTER, OktaConnectorConstants.ATTR_USER_PROFILE_COSTCENTER, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_ORGANIZATION, OktaConnectorConstants.ATTR_USER_PROFILE_ORGANIZATION, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_DIVISION, OktaConnectorConstants.ATTR_USER_PROFILE_DIVISION, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_DEPARTMENT, OktaConnectorConstants.ATTR_USER_PROFILE_DEPARTMENT, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_MANAGERID, OktaConnectorConstants.ATTR_USER_PROFILE_MANAGERID, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_PROFILE_MANAGER, OktaConnectorConstants.ATTR_USER_PROFILE_MANAGER, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_QUESTION, OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_QUESTION, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_ANSWER, OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_ANSWER, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		for (String customAttribute : customAttrList) {
			provAttributes.add(new ProvisioningAttributes(customAttribute, customAttribute, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		}
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.OKTA_USERID, OktaConnectorConstants.OKTA_USERID, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_ROLE_ID, OktaConnectorConstants.ATTR_ROLE_ID, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_ROLE_NAME, OktaConnectorConstants.ATTR_ROLE_NAME, false, OktaConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(OktaConnectorConstants.ATTR_ROLE_DESC, OktaConnectorConstants.ATTR_ROLE_DESC, false, OktaConnectorConstants.ATTR_TYPE_STRING));

		return provAttributes;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getAttributes(Map params) throws Exception {
		return null;
	}

	/**
	 * 
	 * soori - 8:23:42 pm
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#isUserProvisioned(java.lang.String)
	 */
	public ISystemInformation isUserProvisioned(String userId) throws Exception {
		logger.debug(CLASS_NAME + " isUserProvisioned(): Checking isUserProvisioned  with  User Id  : " + userId);
		ISystemInformation systemInformation = new SystemInformation(false);
		try {
			com.alnt.connector.provisioning.model.User userInSystem = getUserbyId(userId);
			if (null != userInSystem && (userInSystem.getStatus().equals(UserStatus.PROVISIONED) || (userInSystem.getStatus().equals(UserStatus.ACTIVE)) || userInSystem.getStatus().equals(UserStatus.SUSPENDED))
					|| (userInSystem.getStatus().equals(UserStatus.LOCKED_OUT)) || (userInSystem.getStatus().equals(UserStatus.DEPROVISIONED)) || (userInSystem.getStatus().equals(UserStatus.STAGED))
					|| (userInSystem.getStatus().equals(UserStatus.RECOVERY)) || (userInSystem.getStatus().equals(UserStatus.PASSWORD_EXPIRED))) {
				systemInformation.setProvisioned(true);
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " isUserProvisioned(): Error while call to external system", e);
			systemInformation.setProvisioned(false);
		}
		return systemInformation;
	}

	/**
	 * 
	 * soori - 9:55:29 pm
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#isUserLocked(java.lang.String)
	 */
	@Override
	public boolean isUserLocked(String userId) throws Exception {
		User user = getUserbyId(userId);
		if (null != user && user.getStatus().equals(UserStatus.SUSPENDED)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * soori - 10:33:36 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#create(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 */

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public IProvisioningResult create(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.debug(CLASS_NAME + " create(): Start of Create Method.");
		IProvisioningResult provResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CREATE_USER, IProvisoiningConstants.CREATE_USER_FAILURE, true, "User is not provisioned !!!");
		boolean userCreated = true;
		OutputStream outputStream = null;
		try {
			ConnectorUtil.logInputParamters(parameters, excludeLogAttrList, logger);
			UserCredentials credentials = new UserCredentials();
			if (parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD) != null && !parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD).toString().isEmpty()) {
				String wordPass = parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD).toString();
				credentials.setPassword(new PasswordCredential(wordPass));
				parameters.remove(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD);
			}
			if (parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_QUESTION) != null && !parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_QUESTION).toString().isEmpty()) {

				RecoveryQuestionCredential rqC = credentials.getRecovery_question();
				if (null == rqC) {
					rqC = new RecoveryQuestionCredential();
				}
				rqC.setQuestion(parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_QUESTION).toString());
				credentials.setRecovery_question(rqC);
				parameters.remove(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_QUESTION);
			}
			if (parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_ANSWER) != null && !parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_ANSWER).toString().isEmpty()) {
				RecoveryQuestionCredential rqC = credentials.getRecovery_question();
				if (null == rqC) {
					rqC = new RecoveryQuestionCredential();
				}
				rqC.setAnswer(parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_ANSWER).toString());
				credentials.setRecovery_question(rqC);
				parameters.remove(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_ANSWER);
			}
			// initially they will pass Alert UserId , but It will be ignored
			if (parameters.get(OktaConnectorConstants.OKTA_USERID) != null && !parameters.get(OktaConnectorConstants.OKTA_USERID).toString().isEmpty()) {
				parameters.remove(OktaConnectorConstants.OKTA_USERID);
			}
			credentials.setProvider(new AuthenticationProvider(true));
			User usertoCreate = new User(credentials, parameters);
			String endPoint = _baseUrl.concat(OktaConnectorConstants.ENDPOINT_CREATE_USER);
			if (_activateUserWhileCreate) {
				endPoint = endPoint.concat(OktaConnectorConstants.TRUE_STRING);
			} else {
				endPoint = endPoint.concat(OktaConnectorConstants.FALSE_STRING);
			}
			OktaURLConnection oktaConnection = OktaClientHelper.getConnection(endPoint, OktaConnectorConstants.REQ_METHOD_TYPE_POST, _idToken);
			Gson gson = new Gson();
			String gsonString = gson.toJson(usertoCreate);
			logger.debug(CLASS_NAME + "   create()   request payload  :::  " + gsonString);
			outputStream = oktaConnection.getOutputStream();
			outputStream.write(gsonString.getBytes());
			outputStream.flush();
			if (oktaConnection.getResponseCode() != HttpStatus.SC_OK) {
				logger.error(CLASS_NAME + "   create()   " + oktaConnection.getResponseMessage());
				provResult.setMsgDesc(oktaConnection.getResponseMessage());
				return provResult;
			} else if (oktaConnection.getResponseCode() == HttpStatus.SC_OK) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((oktaConnection.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					Gson gsonParser = new GsonBuilder().setDateFormat(OktaConnectorConstants.DEFAULT_DATE_FORMAT_SECONDS).create();
					User user = gsonParser.fromJson(output, User.class);
					if (!user.getId().isEmpty()) {
						provResult.setUserId(user.getId());
						provResult.setUserCreated(userCreated);
						provResult.setMsgCode(IProvisoiningConstants.CREATE_USER_SUCCESS);
						provResult.setMsgDesc("User provisioned successfully");
						provResult.setProvFailed(false);
						parameters.put(externalUserIdAttribute, user.getId());
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + "create()", e);
			provResult.setMsgDesc(e.getMessage());
			return provResult;
		}
		logger.debug(CLASS_NAME + " create(): End of Create Method.");
		return provResult;
	}

	/**
	 * 
	 * soori -10-Dec-2019 - 5:17:17 pm
	 * 
	 * updates first name , last name , member of all sites , status, dates , image(replace)
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#update(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult update(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " update(): Start of update method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "update user failed in target system!");
		OutputStream outputStream = null;
		try {
			ConnectorUtil.logInputParamters(parameters, excludeLogAttrList, logger);
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " update() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIdToUpdate = parameters.get(externalUserIdAttribute).toString();
				User user = getUserbyId(userIdToUpdate);
				if (null != user) {

					UserCredentials credentials = null;
					if (parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD) != null && !parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD).toString().isEmpty()) {
						if (null == credentials) {
							credentials = new UserCredentials();
						}
						String wordPass = parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD).toString();
						credentials.setPassword(new PasswordCredential(wordPass));
						parameters.remove(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD);
					}
					if (parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_QUESTION) != null && !parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_QUESTION).toString().isEmpty()) {
						if (null == credentials) {
							credentials = new UserCredentials();
						}
						RecoveryQuestionCredential rqC = credentials.getRecovery_question();
						if (null == rqC) {
							rqC = new RecoveryQuestionCredential();
						}
						rqC.setQuestion(parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_QUESTION).toString());
						credentials.setRecovery_question(rqC);
						parameters.remove(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_QUESTION);
					}
					if (parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_ANSWER) != null && !parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_ANSWER).toString().isEmpty()) {
						if (null == credentials) {
							credentials = new UserCredentials();
						}
						RecoveryQuestionCredential rqC = credentials.getRecovery_question();
						if (null == rqC) {
							rqC = new RecoveryQuestionCredential();
						}
						rqC.setAnswer(parameters.get(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_ANSWER).toString());
						credentials.setRecovery_question(rqC);
						parameters.remove(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_ANSWER);
					}

					Map<String, String> userProfile = new HashMap<String, String>();
					for (String sytemDefinedOptinalAttribute : OktaConnectorConstants.SYSTEM_DEFINED_ATTRIBTESS) {
						if (parameters.get(sytemDefinedOptinalAttribute) != null && !parameters.get(sytemDefinedOptinalAttribute).toString().isEmpty()) {
							userProfile.put(sytemDefinedOptinalAttribute, parameters.get(sytemDefinedOptinalAttribute).toString());
						}
					}
					for (String customAttribute : customAttrList) {
						if (parameters.get(customAttribute) != null && !parameters.get(customAttribute).toString().isEmpty()) {
							userProfile.put(customAttribute, parameters.get(customAttribute).toString());
						}
					}
					User updatedUser = new User();
					updatedUser.setProfile(userProfile);
					if (null != credentials) {
						updatedUser.setCredentials(credentials);
					}
					// NOTE FROM API :: Important: Use the POST method for partial updates. Unspecified properties are set to null with PUT.
					String endPoint = _baseUrl.concat(OktaConnectorConstants.ENDPOINT_GET_USER).concat("/").concat(userIdToUpdate);
					OktaURLConnection oktaConnection = OktaClientHelper.getConnection(endPoint, OktaConnectorConstants.REQ_METHOD_TYPE_POST, _idToken);
					Gson gson = new Gson();
					String gsonString = gson.toJson(updatedUser);
					logger.debug(CLASS_NAME + "   update()   request payload  :::  " + gsonString);
					outputStream = oktaConnection.getOutputStream();
					outputStream.write(gsonString.getBytes());
					outputStream.flush();
					if (oktaConnection.getResponseCode() != HttpStatus.SC_OK) {
						logger.error(CLASS_NAME + "   update()   " + oktaConnection.getResponseMessage());
						provisioningResult.setMsgDesc(oktaConnection.getResponseMessage());
						return provisioningResult;
					} else if (oktaConnection.getResponseCode() == HttpStatus.SC_OK) {
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_SUCCESS, false, null);
					}
				} else {
					if (!_showProvisioningWarnings) {
						logger.debug(CLASS_NAME + " update() user does not exist / already updated in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "User does not exist in external system");
					} else {
						logger.debug(CLASS_NAME + " update() user does not exist / already updated , but warnings are disabled , returning as updated successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_SUCCESS, false, null);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " update()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "Error during update the user");
		}
		logger.info(CLASS_NAME + " update(): End of update method");
		return provisioningResult;
	}

	/**
	 * 
	 * 
	 * This method is to delete the user account in external system.
	 * 
	 * soori - 2:16:34 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#deleteAccount(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult deleteAccount(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.debug(CLASS_NAME + " deleteAccount(): Start of update method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "Error while deleting User in external system");
		ISystemInformation systemInformation = null;
		try {
			ConnectorUtil.logInputParamters(parameters, excludeLogAttrList, logger);
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " deleteAccount() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIDtoDelete = parameters.get(externalUserIdAttribute).toString();
				systemInformation = isUserProvisioned(userIDtoDelete);
				if (systemInformation != null && systemInformation.isProvisioned()) {
					// TODO :: Delete User Deletes a user permanently. This operation can only be performed on users that have a `DEPROVISIONED` status. **This action cannot be recovered!**
					String endPoint = _baseUrl.concat(OktaConnectorConstants.ENDPOINT_GET_USER).concat("/").concat(userIDtoDelete);
					OktaURLConnection oktaConnection = OktaClientHelper.getConnection(endPoint, OktaConnectorConstants.REQ_METHOD_TYPE_DELETE, _idToken);
					if (oktaConnection.getResponseCode() == HttpStatus.SC_OK || oktaConnection.getResponseCode() == HttpStatus.SC_NO_CONTENT) {
						logger.debug(CLASS_NAME + " deleteAccount() user deleted successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_SUCCESS, false, null);
					}
				} else {
					if (!_showProvisioningWarnings) {
						logger.debug(CLASS_NAME + " deleteAccount() user does not exist in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "User does not exist in external system");
					} else {
						logger.debug(CLASS_NAME + " deleteAccount() user does not exist , but warnings are disabled , returning as deleted successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_SUCCESS, false, null);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " deleteAccount(): Error during update the user ", e);
			logger.error(CLASS_NAME + " deleteAccount()", e);
		}
		return provisioningResult;
	}

	/**
	 * 
	 * soori 2019-12-06 - 11:54:34 am
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#lock(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult lock(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {

		logger.info(CLASS_NAME + " lock(): Start of lock method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "lock user failed in target system!");
		OutputStream outputStream = null;
		try {
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " lock() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIdToLock = parameters.get(externalUserIdAttribute).toString();
				User user = getUserbyId(userIdToLock);
				if (null != user) {
					// TODO :: STAGED , PRVISIONED, DEPROVISIONED USERS ARE NOT ALLOWED TO LOCK , THEY WILL CONSIDERED AS NOT ACTIVE TO LOCK
					if (user.getStatus() != UserStatus.SUSPENDED && user.getStatus() == UserStatus.ACTIVE) {

						String endPoint = _baseUrl.concat(OktaConnectorConstants.ENDPOINT_GET_USER).concat("/").concat(userIdToLock).concat("/lifecycle/suspend");
						OktaURLConnection oktaConnection = OktaClientHelper.getConnection(endPoint, OktaConnectorConstants.REQ_METHOD_TYPE_POST, _idToken);
						Gson gson = new Gson();
						String gsonString = gson.toJson(null);
						logger.debug(CLASS_NAME + "   lock()   request payload  :::  " + gsonString);
						outputStream = oktaConnection.getOutputStream();
						outputStream.write(gsonString.getBytes());
						outputStream.flush();
						if (oktaConnection.getResponseCode() != HttpStatus.SC_OK) {
							logger.error(CLASS_NAME + "   lock()   " + oktaConnection.getResponseMessage());
							provisioningResult.setMsgDesc(oktaConnection.getResponseMessage());
							return provisioningResult;
						} else if (oktaConnection.getResponseCode() == HttpStatus.SC_OK) {

							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_SUCCESS, false, null);
						}
					} else {
						if (!_showProvisioningWarnings) {
							logger.debug(CLASS_NAME + "User already locked in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "User already locked in the system");
						} else {
							logger.debug(CLASS_NAME + "User already locked in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_SUCCESS, true, "User already locked in the system");
						}
					}
				} else {
					logger.debug(CLASS_NAME + "lock() user does not exist");
					provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, false, null);
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " lock()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "Error during lock the user");
		}
		logger.info(CLASS_NAME + " lock(): End of lock method");
		return provisioningResult;

	}

	/**
	 * 
	 * soori - 4:43:19 pm if request has card holder status as 2(date based , ie, card status in not NULL and should be integer value) request should pass valid from and valid to
	 * 
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#unlock(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult unlock(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " unlock(): Start of unlock method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "unlock user failed in target system!");
		OutputStream outputStream = null;
		try {
			ConnectorUtil.logInputParamters(parameters, excludeLogAttrList, logger);
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " unlock() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIdToUnlock = parameters.get(externalUserIdAttribute).toString();
				User user = getUserbyId(userIdToUnlock);
				if (null != user) {
					// TODO :: STAGED , PRVISIONED, DEPROVISIONED USERS ARE NOT ALLOWED TO LOCK , THEY WILL CONSIDERED AS SUSPENDED TO UNLOCK
					if (user.getStatus() != UserStatus.ACTIVE && user.getStatus() == UserStatus.SUSPENDED) {
						String endPoint = _baseUrl.concat(OktaConnectorConstants.ENDPOINT_GET_USER).concat("/").concat(userIdToUnlock).concat("/lifecycle/unsuspend");
						OktaURLConnection oktaConnection = OktaClientHelper.getConnection(endPoint, OktaConnectorConstants.REQ_METHOD_TYPE_POST, _idToken);
						Gson gson = new Gson();
						String gsonString = gson.toJson(null);
						logger.debug(CLASS_NAME + "   unlock()   request payload  :::  " + gsonString);
						outputStream = oktaConnection.getOutputStream();
						outputStream.write(gsonString.getBytes());
						outputStream.flush();
						if (oktaConnection.getResponseCode() != HttpStatus.SC_OK) {
							logger.error(CLASS_NAME + "   unlock()   " + oktaConnection.getResponseMessage());
							provisioningResult.setMsgDesc(oktaConnection.getResponseMessage());
							return provisioningResult;
						} else if (oktaConnection.getResponseCode() == HttpStatus.SC_OK) {
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_SUCCESS, false, null);
						}
					} else {
						if (!_showProvisioningWarnings) {
							logger.debug(CLASS_NAME + " unlock() user already unlocked in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "User already active in the system");
						} else {
							logger.debug(CLASS_NAME + " unlock() user already unlocked in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_SUCCESS, true, "User already active in the system");
						}
					}
				} else {
					logger.debug(CLASS_NAME + " unlock() user does not exist");
					provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, false, "User does not exist in external system");
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " unlock()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "Error during unlock the user");
		}
		logger.info(CLASS_NAME + " unlock(): End of lock method");
		return provisioningResult;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult delimitUser(Long requestNumber, List roles, Map parameters, List requestDetails, String action, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " delimitUser(): Start of delimitUser method");
		String failureCode = null, successCode = null;
		successCode = action.equals(IProvisoiningConstants.PROV_ACTION_DELIMIT_USER) ? IProvisoiningConstants.DELIMIT_USER_SUCCESS : IProvisoiningConstants.CHANGE_VALIDITY_DATES_SUCCESS;
		failureCode = action.equals(IProvisoiningConstants.PROV_ACTION_DELIMIT_USER) ? IProvisoiningConstants.DELIMIT_USER_FAILURE : IProvisoiningConstants.CHANGE_VALIDITY_DATES_FAILURE;
		IProvisioningResult provisioningResult = prepareProvisioningResult(action, failureCode, true, " unsuccessfull !!");
		try {
			ConnectorUtil.logInputParamters(parameters, excludeLogAttrList, logger);
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " delimitUser() userId does not passed!!");
				return prepareProvisioningResult(action, failureCode, true, "userId does not passed!!");
			}
			// Delimit user , change validity dates
			String userIdToUpdate = parameters.get(externalUserIdAttribute).toString();
			User user = getUserbyId(userIdToUpdate);
			if (null != user) {
				provisioningResult = prepareProvisioningResult(action, successCode, false, null);
			} else {
				if (!_showProvisioningWarnings) {
					logger.debug(CLASS_NAME + " delimitUser() user does not exist ");
					provisioningResult = prepareProvisioningResult(action, failureCode, true, "User does not exist in external system");
				} else {
					logger.debug(CLASS_NAME + " delimitUser() user does not exist , but warnings are disabled , returning as delimited successfully in the system");
					provisioningResult = prepareProvisioningResult(action, successCode, false, null);
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " delimitUser(): Error during delimitUser the user ", e);
			logger.error(CLASS_NAME + " delimitUser()", e);
			provisioningResult = prepareProvisioningResult(action, failureCode, true, "Error during delimitUser user");
		}
		logger.info(CLASS_NAME + " delimitUser(): End of delimitUser method");
		return provisioningResult;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult activateBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult deActivateBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult addBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult addTempBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	/**
	 * 
	 * soori 2019-12-02 - 5:38:47 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param deprovisionRole
	 * @param attMappin
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#changeAccess(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public IProvisioningResult changeAccess(Long requestNumber, List provisionRoles, Map parameters, List requestDetails, List<IRoleInformation> deprovisionRoles, Map<String, String> attMapping) throws Exception {
		logger.debug(CLASS_NAME + " changeAccess(): Start of changeAccess method");
		IProvisioningResult provisioningResult = null;
		List<IRoleAuditInfo> roleAuditInfoList = new ArrayList<IRoleAuditInfo>();
		try {
			ConnectorUtil.logInputParamters(parameters, excludeLogAttrList, logger);
			if ((deprovisionRoles != null && !deprovisionRoles.isEmpty()) || (provisionRoles != null && !provisionRoles.isEmpty())) {
				if (parameters.get(externalUserIdAttribute) == null) {
					logger.debug(CLASS_NAME + " changeAccess() userId does not passed!!");
					provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "UserId does not passed!!");
				} else {
					String userIdToChangeAccess = parameters.get(externalUserIdAttribute).toString();
					User user = getUserbyId(userIdToChangeAccess);
					if (null != user) {
						List<Group> groups = getUserGroups(userIdToChangeAccess);
						if (deprovisionRoles != null && !deprovisionRoles.isEmpty()) {
							Iterator<IRoleInformation> removeRoleItr = deprovisionRoles.iterator();
							while (removeRoleItr.hasNext()) {
								IRoleInformation roleInformation = (IRoleInformation) removeRoleItr.next();
								for (Group grp : groups) {
									if (grp.getId().equals(roleInformation.getName())) {
										logger.trace(CLASS_NAME + " changeAccess() Removing Existing Role  : " + roleInformation.getName());
										if (removeUserFromGroup(grp.getId(), userIdToChangeAccess)) {
											roleAuditInfoList.add(getRoleAuditInfo(roleInformation, IProvisoiningConstants.REMOVE_ROLE));
										} else {
											provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "Error while deleting Role");
											return provisioningResult;
										}
									}
								}
							}
						}
						if (provisionRoles != null && !provisionRoles.isEmpty()) {
							Iterator<IRoleInformation> addRoleItr = provisionRoles.iterator();
							while (addRoleItr.hasNext()) {
								IRoleInformation roleInformation = (IRoleInformation) addRoleItr.next();
								logger.trace(CLASS_NAME + " changeAccess() Adding new role to user is  : " + roleInformation.getName());
								Boolean hasUserThisRole = false;
								for (Group grp : groups) {
									if (grp.getId().equals(roleInformation.getName())) {
										hasUserThisRole = true;
									}
								}
								if (!hasUserThisRole) {
									if (addUserToGroup(roleInformation.getName(), userIdToChangeAccess)) {
										roleAuditInfoList.add(getRoleAuditInfo(roleInformation, IProvisoiningConstants.ADD_ROLE));
									} else {
										provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "Error while Adding Role");
										return provisioningResult;
									}
								}
							}
						}
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_SUCCESS, false, "Access changed successfully.");
					} else {
						if (!_showProvisioningWarnings) {
							logger.debug(CLASS_NAME + " changeAccess() user does not exist ");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "User does not exist in external system");
						} else {
							logger.debug(CLASS_NAME + " changeAccess() user does not exist , but warnings are disabled , returning as change Access successfully in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_SUCCESS, false, null);
						}
					}
				}
			} else {
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_SUCCESS, false, "Access changed successfully.");
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " changeAccess(): Error Assigning the roles = ", e);
			roleAuditInfoList = new ArrayList<IRoleAuditInfo>();
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "Error assigning roles" + e.getMessage());
		}
		provisioningResult.setRoleList(roleAuditInfoList);
		return provisioningResult;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult changeBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult changeBadgeRoles(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult removeBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	/**
	 * 
	 * soori 2019-12-06 - 12:01:43 pm
	 * 
	 * @param roleInformation
	 * @param action
	 * @return IRoleAuditInfo
	 *
	 */
	private IRoleAuditInfo getRoleAuditInfo(IRoleInformation roleInformation, String action) {
		IRoleAuditInfo roleAuditInfo = new RoleAuditInfo();
		roleAuditInfo.setRoleName(roleInformation.getName());
		roleAuditInfo.setValidFrom(roleInformation.getValidFrom());
		roleAuditInfo.setValidTo(roleInformation.getValidTo());
		roleAuditInfo.setAction(action);
		return roleAuditInfo;
	}

	/**
	 * 
	 * soori 2019-12-06 - 11:55:00 am
	 * 
	 * @param dateString
	 * @param currentFormat
	 * @param exceptedFormat
	 * @return String
	 * @throws Exception
	 *
	 */
	private String parseDateStringToDateString(String dateString, String currentFormat, String exceptedFormat) throws Exception {
		try {
			logger.debug(CLASS_NAME + " parseDateString() dateString to parse :: " + dateString);
			logger.debug(CLASS_NAME + " parseDateString() dateString  current format :: " + dateString);
			logger.debug(CLASS_NAME + " parseDateString() dateString  exceptedFormat :: " + exceptedFormat);
			DateTimeFormatter oldPattern = DateTimeFormatter.ofPattern(currentFormat);
			DateTimeFormatter newPattern = DateTimeFormatter.ofPattern(exceptedFormat);
			LocalDateTime datetime = LocalDateTime.parse(dateString, oldPattern);
			return datetime.format(newPattern);
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * 
	 * soori 2019-12-06 - 11:55:08 am
	 * 
	 * @param date
	 * @param dateFormat
	 * @return Date
	 * @throws ParseException
	 *
	 */
	@SuppressWarnings("unused")
	private static final Date parseStringToDate(String date, String dateFormat) throws ParseException {
		if (dateFormat == null || dateFormat.equals("")) {
			dateFormat = OktaConnectorConstants.DEFAULT_DATE_FORMAT;
		}
		SimpleDateFormat formater = new SimpleDateFormat(dateFormat);
		return formater.parse(date);
	}

	/**
	 * 
	 * soori 2019-12-06 - 11:55:26 am
	 * 
	 * @param date
	 * @return String
	 * @throws ParseException
	 *
	 */
	@SuppressWarnings("unused")
	private static final String parseDateOktaStringFormat(Date date) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat(OktaConnectorConstants.DEFAULT_DATE_FORMAT);
		return dateFormat.format(date);
	}

	private IProvisioningResult prepareProvisioningResult(String provisioningAction, String msgCode, Boolean isFailed, String msgDesc) {
		IProvisioningResult provisioningResult = new ProvisioningResult();
		provisioningResult.setMsgCode(msgCode);
		if (null != isFailed) {
			provisioningResult.setProvFailed(isFailed);
		}
		provisioningResult.setMsgDesc(msgDesc);
		provisioningResult.setProvAction(provisioningAction);
		return provisioningResult;
	}

	private User getUserbyId(String userId) throws Exception {
		try {
			String endPoint = _baseUrl.concat(OktaConnectorConstants.ENDPOINT_GET_USER).concat("/").concat(userId);
			OktaURLConnection oktaConnection = OktaClientHelper.getConnection(endPoint, OktaConnectorConstants.REQ_METHOD_TYPE_GET, _idToken);
			if (oktaConnection.getResponseCode() == HttpStatus.SC_OK) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((oktaConnection.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					Gson gsonParser = new GsonBuilder().setDateFormat(OktaConnectorConstants.DEFAULT_DATE_FORMAT_SECONDS).create();
					return gsonParser.fromJson(output, User.class);
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " getUserbyId(): Error while call to external system", e);
			return null;
		}
		return null;
	}

	@Override
	public void setTaskId(Long arg0) {
	}

	@Override
	public IProvisioningResult updatePassword(Long requestNumber, String userId, String origPwd, String newPwd) throws Exception {
		IProvisioningResult provResult = new ProvisioningResult();
		provResult.setProvAction(IProvisoiningConstants.PROV_ACTION_CHANGE_USER);
		ISystemInformation systemInformation = null;
		OutputStream outputStream = null;
		try {
			systemInformation = isUserProvisioned(userId);
			if (systemInformation != null && systemInformation.isProvisioned()) {
				if (newPwd != null && !newPwd.isEmpty()) {
					UserCredentials credentials = new UserCredentials();
					credentials.setPassword(new PasswordCredential(newPwd));
					User userPwdUpdate = new User();
					userPwdUpdate.setCredentials(credentials);
					String endPoint = _baseUrl.concat(OktaConnectorConstants.ENDPOINT_GET_USER).concat("/").concat(userId);
					OktaURLConnection oktaConnection = OktaClientHelper.getConnection(endPoint, OktaConnectorConstants.REQ_METHOD_TYPE_POST, _idToken);
					Gson gson = new Gson();
					String gsonString = gson.toJson(userId);
					logger.debug(CLASS_NAME + "   updatePassword()   request payload  :::  " + gsonString);
					outputStream = oktaConnection.getOutputStream();
					outputStream.write(gsonString.getBytes());
					outputStream.flush();
					if (oktaConnection.getResponseCode() != HttpStatus.SC_OK) {
						logger.error(CLASS_NAME + "   updatePassword()   " + oktaConnection.getResponseMessage());
						provResult.setMsgDesc(oktaConnection.getResponseMessage());
						return provResult;
					} else if (oktaConnection.getResponseCode() == HttpStatus.SC_OK) {
						provResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_SUCCESS, false, null);
					}
				} else {
					provResult.setMsgCode(IProvisoiningConstants.CHANGE_USER_FAILURE);
					provResult.setMsgDesc("New Password is Empty");
					provResult.setProvFailed(true);
					return provResult;
				}
			} else {
				provResult.setMsgCode(IProvisoiningConstants.CHANGE_USER_FAILURE);
				provResult.setMsgDesc("User doesn’t exist Target system");
				provResult.setProvFailed(true);
				return provResult;
			}
		} catch (Exception e) {
			provResult.setMsgCode(IProvisoiningConstants.CHANGE_USER_FAILURE);
			provResult.setMsgDesc(e.getMessage());
			provResult.setProvFailed(true);
			return provResult;
		}
		return provResult;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getBadgeLastLocation(String arg0) throws Exception {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getDetailsAsList(String arg0, String arg1) throws Exception {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getExistingBadges(String arg0) throws Exception {

		return null;
	}

	@Override
	public IProvisioningStatus getProvisioningStatus(Map<String, String> arg0) throws Exception {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List provision(Long arg0, String arg1, List arg2, List<com.alnt.fabric.component.rolemanagement.search.IRoleInformation> arg3, Map arg4, List arg5, List<com.alnt.fabric.component.rolemanagement.search.IRoleInformation> arg6,
			Map<String, String> arg7) throws Exception {
		return null;
	}

	@SuppressWarnings("rawtypes")
	public IProvisioningResult create(Long arg0, List arg1, Map arg2, List arg3, Map<String, String> arg4, Map<String, String> arg5) throws Exception {
		return null;
	}

	// comments = "PUT - /api/v1/groups/{groupId}/users/{userId}")
	boolean addUserToGroup(String groupId, String userId) {
		try {
			String endPoint = _baseUrl.concat(OktaConnectorConstants.ENDPOINT_GROUPS).concat("/").concat(groupId).concat("/").concat(OktaConnectorConstants.ENDPOINT_GET_USER).concat("/").concat(userId);
			OktaURLConnection oktaConnection = OktaClientHelper.getConnection(endPoint, OktaConnectorConstants.REQ_METHOD_TYPE_PUT, _idToken);
			if (oktaConnection.getResponseCode() == HttpStatus.SC_OK || oktaConnection.getResponseCode() == HttpStatus.SC_NO_CONTENT) {
				return true;
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " addUserToGroup(): Error while add User To Group", e);
			return false;
		}
		return false;
	};

	// comments = "DELETE - /api/v1/groups/{groupId}/users/{userId}")
	boolean removeUserFromGroup(String groupId, String userId) {
		try {
			String endPoint = _baseUrl.concat(OktaConnectorConstants.ENDPOINT_GROUPS).concat("/").concat(groupId).concat("/").concat(OktaConnectorConstants.ENDPOINT_GET_USER).concat("/").concat(userId);
			OktaURLConnection oktaConnection = OktaClientHelper.getConnection(endPoint, OktaConnectorConstants.REQ_METHOD_TYPE_DELETE, _idToken);
			if (oktaConnection.getResponseCode() == HttpStatus.SC_OK || oktaConnection.getResponseCode() == HttpStatus.SC_NO_CONTENT) {
				return true;
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " removeUserFromGroup(): Error while remove User From Group", e);
			return false;
		}
		return false;

	};

	// comments = "GET - /api/v1/users/{userId}/groups")
	List<Group> getUserGroups(String userId) {
		try {
			String endPoint = _baseUrl.concat(OktaConnectorConstants.ENDPOINT_GET_USER).concat("/").concat(userId).concat("/").concat(OktaConnectorConstants.ENDPOINT_GROUPS);
			OktaURLConnection oktaConnection = OktaClientHelper.getConnection(endPoint, OktaConnectorConstants.REQ_METHOD_TYPE_GET, _idToken);
			if (oktaConnection.getResponseCode() == HttpStatus.SC_OK) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((oktaConnection.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					JSONArray restapiresponseArray = new JSONArray(output);
					if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
						Type listType = new TypeToken<List<Group>>() {
						}.getType();
						Gson gsonParser = new GsonBuilder().setDateFormat(OktaConnectorConstants.DEFAULT_DATE_FORMAT_SECONDS).create();
						return gsonParser.fromJson(String.valueOf(restapiresponseArray), listType);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " getUserGroups(): Error while getting User Groups", e);
			return null;
		}
		return null;
	}

	// comments = "GET - /api/v1/groups")
	public List<Group> getAllGroups() {
		try {
			String endPoint = _baseUrl.concat(OktaConnectorConstants.ENDPOINT_GROUPS);
			OktaURLConnection oktaConnection = OktaClientHelper.getConnection(endPoint, OktaConnectorConstants.REQ_METHOD_TYPE_GET, _idToken);
			if (oktaConnection.getResponseCode() == HttpStatus.SC_OK) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((oktaConnection.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					JSONArray restapiresponseArray = new JSONArray(output);
					if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
						Type listType = new TypeToken<List<Group>>() {
						}.getType();
						Gson gsonParser = new GsonBuilder().setDateFormat(OktaConnectorConstants.DEFAULT_DATE_FORMAT_SECONDS).create();
						return gsonParser.fromJson(String.valueOf(restapiresponseArray), listType);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " getAllGroups(): Error while getting User Groups", e);
			return null;
		}
		return null;
	}

}
