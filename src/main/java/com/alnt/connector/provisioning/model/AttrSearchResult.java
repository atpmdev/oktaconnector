package com.alnt.connector.provisioning.model;

public class AttrSearchResult implements com.alnt.access.provisioning.model.IAttrSearchResult {

	private String keyFieldValue;
	private String valueFieldValue;

	public AttrSearchResult(String keyFieldValue, String valueFieldValue) {
		this.keyFieldValue = keyFieldValue;
		this.valueFieldValue = valueFieldValue;
	}
	public AttrSearchResult() {
		
	}

	@Override
	public String getKeyFieldValue() {
		return keyFieldValue;
	}

	@Override
	public String getValueFieldValue() {
		return valueFieldValue;
	}

	@Override
	public void setKeyFieldValue(String keyFieldValue) {
		this.keyFieldValue = keyFieldValue;
	}

	@Override
	public void setValueFieldValue(String valueFieldValue) {
		this.valueFieldValue = valueFieldValue;
	}

}
