package com.alnt.connector.provisioning.model;

import java.io.Serializable;

public class GroupProfile implements Serializable {
	
	private static final long serialVersionUID = -6956700923102374572L;
	
	private String description;
	private String name;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}



}
