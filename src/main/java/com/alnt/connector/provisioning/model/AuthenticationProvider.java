package com.alnt.connector.provisioning.model;

import java.io.Serializable;

public class AuthenticationProvider implements Serializable {

	private static final long serialVersionUID = -5481170309481676942L;

	private String name;
	private AuthenticationProviderType type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AuthenticationProviderType getType() {
		return type;
	}

	public void setType(AuthenticationProviderType type) {
		this.type = type;
	}

	public AuthenticationProvider() {

	}

	public AuthenticationProvider(Boolean isOkta) {
		this.name = AuthenticationProviderType.OKTA.toString();
		this.type = AuthenticationProviderType.OKTA;
	}

	public AuthenticationProvider(String name, AuthenticationProviderType type) {
		this.name = name;
		this.type = type;
	}

}
