package com.alnt.connector.provisioning.model;

public enum AuthenticationProviderType {

	  
	  ACTIVE_DIRECTORY("ACTIVE_DIRECTORY"),
	  
	  FEDERATION("FEDERATION"),
	  
	  LDAP("LDAP"),
	  
	  OKTA("OKTA"),
	  
	  SOCIAL("SOCIAL"),
	  
	  IMPORT("IMPORT");

	  private String value;

	  AuthenticationProviderType(String value) {
	    this.value = value;
	  }

	  @Override
	  public String toString() {
	    return String.valueOf(value);
	  }
	}
