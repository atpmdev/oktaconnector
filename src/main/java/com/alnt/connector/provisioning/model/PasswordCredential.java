package com.alnt.connector.provisioning.model;

import java.io.Serializable;

public class PasswordCredential implements Serializable {

	private static final long serialVersionUID = -1817794263525457721L;
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public PasswordCredential() {
		
	}
	
	public PasswordCredential(String value){
		this.value = value;
	}

}
