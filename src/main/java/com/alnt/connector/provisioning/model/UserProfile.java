package com.alnt.connector.provisioning.model;

import java.io.Serializable;
import java.util.LinkedHashMap;

public class UserProfile extends LinkedHashMap<String, Object> implements Serializable {

	private static final long serialVersionUID = -1562450993073277613L;
	private String email;
	private String firstName;
	private String lastName;
	private String login;
	private String mobilePhone;
	private String secondEmail;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getSecondEmail() {
		return secondEmail;
	}

	public void setSecondEmail(String secondEmail) {
		this.secondEmail = secondEmail;
	}

}
