package com.alnt.connector.provisioning.model;

import java.io.Serializable;
import java.util.List;

public class UserCredentials implements Serializable {

	private static final long serialVersionUID = 518441442980090380L;

	private RecoveryQuestionCredential recovery_question;
	private List<EmailAddress> emails;
	private PasswordCredential password;
	private AuthenticationProvider provider;

	public RecoveryQuestionCredential getRecovery_question() {
		return recovery_question;
	}

	public void setRecovery_question(RecoveryQuestionCredential recovery_question) {
		this.recovery_question = recovery_question;
	}

	public List<EmailAddress> getEmails() {
		return emails;
	}

	public void setEmails(List<EmailAddress> emails) {
		this.emails = emails;
	}

	public PasswordCredential getPassword() {
		return password;
	}

	public void setPassword(PasswordCredential password) {
		this.password = password;
	}

	public AuthenticationProvider getProvider() {
		return provider;
	}

	public void setProvider(AuthenticationProvider provider) {
		this.provider = provider;
	}

}
