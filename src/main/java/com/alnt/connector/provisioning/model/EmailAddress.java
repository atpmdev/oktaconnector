package com.alnt.connector.provisioning.model;

import java.io.Serializable;

public class EmailAddress implements Serializable {

	private static final long serialVersionUID = -6487511877080880073L;
	private EmailStatus status;
	private EmailType type;
	private String value;

	public EmailStatus getStatus() {
		return status;
	}

	public void setStatus(EmailStatus status) {
		this.status = status;
	}

	public EmailType getType() {
		return type;
	}

	public void setType(EmailType type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
