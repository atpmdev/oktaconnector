package com.alnt.connector.provisioning.model;

public enum EmailType {
	PRIMARY("PRIMARY"),

	SECONDARY("SECONDARY");

	private String value;

	EmailType(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

}
