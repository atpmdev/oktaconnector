package com.alnt.connector.provisioning.model;

import java.io.Serializable;


public class RecoveryQuestionCredential implements Serializable {
	
	private static final long serialVersionUID = -2369474319593346026L;
	
		private String answer;
	    private String question;
	    
		public String getAnswer() {
			return answer;
		}
		public void setAnswer(String answer) {
			this.answer = answer;
		}
		public String getQuestion() {
			return question;
		}
		public void setQuestion(String question) {
			this.question = question;
		}

	 
}
