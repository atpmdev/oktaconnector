package com.alnt.connector.provisioning.model;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class User implements Serializable {

	private static final long serialVersionUID = 1498071988605207877L;

	private Map<String, Object> embedded;
	Map<String, String> profile = new LinkedHashMap<>();
	private Map<String, Object> links;
	private Date activated;
	private Date created;
	private UserCredentials credentials;
	private String id;
	private Date lastLogin;
	private Date lastUpdated;
	private Date passwordChanged;
	private UserStatus status;
	private Date statusChanged;
	private UserStatus transitioningToStatus;

	public User(){
		
	}
	
	@SuppressWarnings("unchecked")
	public User(UserCredentials credentials, @SuppressWarnings("rawtypes") Map parameters) {
		this.credentials = credentials;
		this.profile = parameters;
	}

	public Map<String, Object> getEmbedded() {
		return embedded;
	}

	public void setEmbedded(Map<String, Object> embedded) {
		this.embedded = embedded;
	}

	public Map<String, Object> getLinks() {
		return links;
	}

	public void setLinks(Map<String, Object> links) {
		this.links = links;
	}

	public Date getActivated() {
		return activated;
	}

	public void setActivated(Date activated) {
		this.activated = activated;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public UserCredentials getCredentials() {
		return credentials;
	}

	public void setCredentials(UserCredentials credentials) {
		this.credentials = credentials;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Date getPasswordChanged() {
		return passwordChanged;
	}

	public void setPasswordChanged(Date passwordChanged) {
		this.passwordChanged = passwordChanged;
	}

	public UserStatus getStatus() {
		return status;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}

	public Date getStatusChanged() {
		return statusChanged;
	}

	public void setStatusChanged(Date statusChanged) {
		this.statusChanged = statusChanged;
	}

	public UserStatus getTransitioningToStatus() {
		return transitioningToStatus;
	}

	public void setTransitioningToStatus(UserStatus transitioningToStatus) {
		this.transitioningToStatus = transitioningToStatus;
	}

	public Map<String, String> getProfile() {
		return profile;
	}

	public void setProfile(Map<String, String> profile) {
		this.profile = profile;
	}

}
