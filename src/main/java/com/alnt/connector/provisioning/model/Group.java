package com.alnt.connector.provisioning.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Group implements Serializable {

	private static final long serialVersionUID = -226594674997106774L;

	private Map<String, Object> embedded;
	private Map<String, Object> links;
	private Date created;
	private String id;
	private Date lastMembershipUpdated;
	private Date lastUpdated;
	private List<String> objectClass;
	// private Map<String, String> profile;
	private GroupProfile profile;
	private String type;

	public Map<String, Object> getEmbedded() {
		return embedded;
	}

	public void setEmbedded(Map<String, Object> embedded) {
		this.embedded = embedded;
	}

	public Map<String, Object> getLinks() {
		return links;
	}

	public void setLinks(Map<String, Object> links) {
		this.links = links;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getLastMembershipUpdated() {
		return lastMembershipUpdated;
	}

	public void setLastMembershipUpdated(Date lastMembershipUpdated) {
		this.lastMembershipUpdated = lastMembershipUpdated;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public List<String> getObjectClass() {
		return objectClass;
	}

	public void setObjectClass(List<String> objectClass) {
		this.objectClass = objectClass;
	}

	public GroupProfile getProfile() {
		return profile;
	}

	public void setProfile(GroupProfile profile) {
		this.profile = profile;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
