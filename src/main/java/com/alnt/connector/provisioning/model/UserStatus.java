package com.alnt.connector.provisioning.model;

public enum UserStatus {

	  
	  STAGED("STAGED"),
	  
	  PROVISIONED("PROVISIONED"),
	  
	  ACTIVE("ACTIVE"),
	  
	  RECOVERY("RECOVERY"),
	  
	  PASSWORD_EXPIRED("PASSWORD_EXPIRED"),
	  
	  LOCKED_OUT("LOCKED_OUT"),
	  
	  DEPROVISIONED("DEPROVISIONED"),
	  
	  SUSPENDED("SUSPENDED");

	  private String value;

	  UserStatus(String value) {
	    this.value = value;
	  }

	  @Override
	  public String toString() {
	    return String.valueOf(value);
	  }
	}