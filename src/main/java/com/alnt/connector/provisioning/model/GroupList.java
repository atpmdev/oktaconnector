package com.alnt.connector.provisioning.model;

import java.io.Serializable;
import java.util.List;

public class GroupList implements Serializable {
	
	private static final long serialVersionUID = 4872519561902211068L;
	
	private List<Group> groups;

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

}
