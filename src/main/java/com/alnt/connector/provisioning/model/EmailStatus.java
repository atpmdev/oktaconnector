package com.alnt.connector.provisioning.model;

public enum EmailStatus {

	  
	  VERIFIED("VERIFIED"),
	  
	  UNVERIFIED("UNVERIFIED");

	  private String value;

	  EmailStatus(String value) {
	    this.value = value;
	  }

	  @Override
	  public String toString() {
	    return String.valueOf(value);
	  }
	}
