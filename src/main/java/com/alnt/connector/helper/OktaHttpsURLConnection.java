package com.alnt.connector.helper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.net.ssl.HttpsURLConnection;

public class OktaHttpsURLConnection implements OktaURLConnection {
	private HttpsURLConnection httpsURLConnection;

	public HttpsURLConnection getHttpsURLConnection() {
		return httpsURLConnection;
	}

	public void setHttpsURLConnection(HttpsURLConnection httpsURLConnection) {
		this.httpsURLConnection = httpsURLConnection;
	}

	public int getResponseCode() throws IOException {
		return httpsURLConnection.getResponseCode();
	}

	public void disconnect() {
		httpsURLConnection.disconnect();
	}

	public OutputStream getOutputStream() throws IOException {
		return httpsURLConnection.getOutputStream();
	}

	public InputStream getInputStream() throws IOException {
		return httpsURLConnection.getInputStream();
	}

	public String getResponseMessage() throws IOException {
		return httpsURLConnection.getResponseMessage();
	}
	
}
