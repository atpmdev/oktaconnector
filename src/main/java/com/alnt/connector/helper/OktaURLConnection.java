package com.alnt.connector.helper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface OktaURLConnection {
	public OutputStream getOutputStream() throws IOException;
	public InputStream getInputStream() throws IOException;
	public String getResponseMessage()throws IOException;
	public int getResponseCode() throws IOException;
	public void disconnect();
}
