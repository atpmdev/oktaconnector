package com.alnt.connector.helper;

import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;

import com.alnt.connector.constants.OktaConnectorConstants;

public class OktaClientHelper {

	final private static String CLASS_NAME = OktaClientHelper.class.getName();
	static Logger logger = Logger.getLogger(CLASS_NAME);

	public static final boolean isHttpsProtocal(String restUrl) {
		return !restUrl.isEmpty() && restUrl.startsWith("https://");
	}

	public static OktaURLConnection getConnection(final String restURL, final String methodType,  String apiKey) {
		if (isHttpsProtocal(restURL)) {
			return getRESTHttpsServiceConnection(restURL.trim(), methodType,  apiKey);
		} else {
			return getRESTHttpServiceConnection(restURL.trim(), methodType,  apiKey);
		}
	}

	public static OktaURLConnection getRESTHttpServiceConnection(final String restURL, final String methodType,  String apiKey) {
		final String METHOD_NAME = "getRESTServiceConnection()";
		logger.debug(CLASS_NAME + "getRESTHttpServiceConnection  :: " + "restUrl" + restURL + "method type: " + methodType);
		OktaHttpURLConnection restHttpURLConnection = new OktaHttpURLConnection();
		HttpURLConnection conn = null;
		URL url = null;
		try {
			logger.debug(CLASS_NAME + METHOD_NAME + "Building URL:" + restURL + " methodType:" + methodType);
			String escapeRestURL = restURL.replaceAll(" ", "%20");
			url = new URL(escapeRestURL);
			logger.debug(CLASS_NAME + METHOD_NAME + "Opening connection");
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod(methodType);
			conn.setRequestProperty(OktaConnectorConstants.ACCEPT_TYPE, OktaConnectorConstants.JSON_CONTENT);
			conn.setRequestProperty(OktaConnectorConstants.CONTENT_TYPE, OktaConnectorConstants.JSON_CONTENT);
			conn.setDoOutput(true);
			conn.setRequestProperty(OktaConnectorConstants.REQ_HEADER_AUTHORIZATION, OktaConnectorConstants.REQ_HEADER_AUTHORIZATION_KEY_PREFIX + apiKey);
			restHttpURLConnection.setHttpURLConnection(conn);
			logger.debug(CLASS_NAME + METHOD_NAME + "returning HttpURLConnection connection");
		} catch (Exception ex) {
			logger.error(CLASS_NAME + METHOD_NAME + "Exception while opening the port : " + ex);
		}
		logger.debug(CLASS_NAME + METHOD_NAME + "ENDS");
		return restHttpURLConnection;
	}

	public static OktaURLConnection getRESTHttpsServiceConnection(final String restURL, final String methodType,  String apiKey) {
		final String METHOD_NAME = "getRESTServiceConnection()";
		logger.debug(CLASS_NAME + METHOD_NAME + "Inside : getRESTServiceConnection ,restUrl" + restURL + "method type: " + methodType);
		OktaHttpsURLConnection restHttpsURLConnection = new OktaHttpsURLConnection();
		HttpsURLConnection conn = null;
		URL url = null;
		try {
			logger.debug(CLASS_NAME + METHOD_NAME + "Building URL:" + restURL + " methodType:" + methodType);
			String escapeRestURL = restURL.replaceAll(" ", "%20");
			url = new URL(escapeRestURL);
			logger.debug(CLASS_NAME + METHOD_NAME + "Opening connection");
			conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod(methodType);
			conn.setRequestProperty(OktaConnectorConstants.ACCEPT_TYPE, OktaConnectorConstants.JSON_CONTENT);
			conn.setRequestProperty(OktaConnectorConstants.CONTENT_TYPE, OktaConnectorConstants.JSON_CONTENT);
			conn.setDoOutput(true);
			conn.setRequestProperty(OktaConnectorConstants.REQ_HEADER_AUTHORIZATION, OktaConnectorConstants.REQ_HEADER_AUTHORIZATION_KEY_PREFIX + apiKey);
			restHttpsURLConnection.setHttpsURLConnection(conn);
			logger.debug(CLASS_NAME + METHOD_NAME + "returning HttpURLConnection connection");
		} catch (Exception ex) {
			logger.error(CLASS_NAME + METHOD_NAME + "Exception while opening the port : " + ex);
		}
		logger.debug(CLASS_NAME + METHOD_NAME + "ENDS");
		return restHttpsURLConnection;
	}
}
