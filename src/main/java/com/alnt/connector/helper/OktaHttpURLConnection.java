package com.alnt.connector.helper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;

public class OktaHttpURLConnection implements OktaURLConnection {
	
	private HttpURLConnection httpURLConnection;

	public HttpURLConnection getHttpURLConnection() {
		return httpURLConnection;
	}

	public void setHttpURLConnection(HttpURLConnection httpURLConnection) {
		this.httpURLConnection = httpURLConnection;
	}
	
	public int getResponseCode() throws IOException {
		return httpURLConnection.getResponseCode();
	}

	public void disconnect() {
		httpURLConnection.disconnect();
	}
	
	public OutputStream getOutputStream() throws IOException {
		return httpURLConnection.getOutputStream();
	}

	public InputStream getInputStream() throws IOException {
		return httpURLConnection.getInputStream();
	}
	
	public String getResponseMessage() throws IOException {
		return httpURLConnection.getResponseMessage();
	}	
}
