package com.alnt.connector.exception;

public class OktaConnectorException extends Exception{

	private static final long serialVersionUID = 410782776743392490L;

	public OktaConnectorException(String message) {
		super(message);
	}
}
