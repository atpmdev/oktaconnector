package com.alnt.connector.constants;

public class OktaConnectorConstants {

	public static final String CONNECTOR_NAME = "OktaConnector";
	

	// CONNECTOR SPECIFIC
	public static final String ALERT_APP_DATE_FORMAT = "alertAppDateFormat";
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	public static final String DEFAULT_DATE_FORMAT_SECONDS = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static final String SHOW_PROVISION_WARNINGS = "showProvisioningWarnings";
	public static final String ORG_URL = "orgUrl";
	public static final String ACTIVATE_USER_WHILE_CREATE = "activateUserWhileCreate";
	public static final String SENSITIVE_ATTRIBUTES = "sensitiveAttributes";
	public static final String CUSTOM_ATTRIBUTES = "customAttributes";
	public static final String ID_TOKEN = "idToken";

	public static final String REQ_HEADER_AUTHORIZATION = "Authorization";
	public static final String AUTHORIZATION_TYPE = "Basic ";
	public static final String USERID_PWD_SEPERATOR = ":";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String ACCEPT_TYPE = "Accept";
	public static final String JSON_CONTENT = "application/json";

	public static final String REQ_HEADER_AUTHORIZATION_KEY_PREFIX = "SSWS ";

	//ENDPOINTS
	public static final String ENDPOINT_TEST = "users?limit=0";
	public static final String ENDPOINT_CREATE_USER = "users?activate=";
	public static final String ENDPOINT_GET_USER = "users";
	public static final String ENDPOINT_GROUPS = "groups";
	public static final String TRUE_STRING = "true";
	public static final String FALSE_STRING = "false";
	// GENERIC METADATA
	public static final String REQ_METHOD_TYPE_GET = "GET";
	public static final String REQ_METHOD_TYPE_POST = "POST";
	public static final String REQ_METHOD_TYPE_HEAD = "HEAD";
	public static final String REQ_METHOD_TYPE_OPTIONS = "OPTIONS";
	public static final String REQ_METHOD_TYPE_PUT = "PUT";
	public static final String REQ_METHOD_TYPE_DELETE = "DELETE";
	public static final String REQ_METHOD_TYPE_TRACE = "TRACE";
	public static final String REQ_METHOD_TYPE_PATCH = "PATCH";

	public static final String ATTR_TYPE_STRING = "String";
	public static final String ATTR_TYPE_INT = "Int";
	public static final String ATTR_TYPE_BOOLEAN = "Boolean";
	public static final String ATTR_TYPE_DATE = "Date";
	
	public static final String OKTA_USERID = "UserId";
	public static final String ALERT_USERID = "UserId";

	public static final String ATTR_ROLE_ID = "GroupId";
	public static final String ATTR_ROLE_NAME = "GroupName";
	public static final String ATTR_ROLE_DESC = "GroupDescription";
	public static final String ALERT_ROLE_NAME = "RoleName";

	// CONNECTOR ATTRIBUTES
	public static final String ATTR_USER_PROFILE_LOGIN = "login";
	public static final String ATTR_USER_PROFILE_EMAIL = "email";
	public static final String ATTR_USER_PROFILE_SECONDEMAIL = "secondEmail";
	public static final String ATTR_USER_PROFILE_FIRSTNAME = "firstName";
	public static final String ATTR_USER_PROFILE_LASTNAME = "lastName";
	public static final String ATTR_USER_PROFILE_MOBILEPHONE = "mobilePhone";

	public static final String ATTR_USER_PROFILE_MIDDLENAME = "middleName";
	public static final String ATTR_USER_PROFILE_HONORIFICPREFIX = "honorificPrefix";
	public static final String ATTR_USER_PROFILE_HONORIFICSUFFIX = "honorificSuffix";
	public static final String ATTR_USER_PROFILE_TITLE = "title";
	public static final String ATTR_USER_PROFILE_DISPLAYNAME = "displayName";
	public static final String ATTR_USER_PROFILE_NICKNAME = "nickName";
	public static final String ATTR_USER_PROFILE_PROFILEURL = "profileUrl";
	public static final String ATTR_USER_PROFILE_PRIMARYPHONE = "primaryPhone";
	public static final String ATTR_USER_PROFILE_STREETADDRESS = "streetAddress";
	public static final String ATTR_USER_PROFILE_CITY = "city";
	public static final String ATTR_USER_PROFILE_STATE = "state";
	public static final String ATTR_USER_PROFILE_ZIPCODE = "zipCode";
	public static final String ATTR_USER_PROFILE_COUNTRYCODE = "countryCode";
	public static final String ATTR_USER_PROFILE_POSTALADDRESS = "postalAddress";
	public static final String ATTR_USER_PROFILE_PREFERREDLANGUAGE = "preferredLanguage";
	public static final String ATTR_USER_PROFILE_LOCALE = "locale";
	public static final String ATTR_USER_PROFILE_TIMEZONE = "timezone";
	public static final String ATTR_USER_PROFILE_USERTYPE = "userType";
	public static final String ATTR_USER_PROFILE_EMPLOYEENUMBER = "employeeNumber";
	public static final String ATTR_USER_PROFILE_COSTCENTER = "costCenter";
	public static final String ATTR_USER_PROFILE_ORGANIZATION = "organization";
	public static final String ATTR_USER_PROFILE_DIVISION = "division";
	public static final String ATTR_USER_PROFILE_DEPARTMENT = "department";
	public static final String ATTR_USER_PROFILE_MANAGERID = "managerId";
	public static final String ATTR_USER_PROFILE_MANAGER = "manager";

	public static final String[] SYSTEM_DEFINED_ATTRIBTESS = { ATTR_USER_PROFILE_MIDDLENAME, ATTR_USER_PROFILE_HONORIFICPREFIX, ATTR_USER_PROFILE_HONORIFICSUFFIX, ATTR_USER_PROFILE_TITLE, ATTR_USER_PROFILE_DISPLAYNAME,
			ATTR_USER_PROFILE_NICKNAME, ATTR_USER_PROFILE_PROFILEURL, ATTR_USER_PROFILE_PRIMARYPHONE, ATTR_USER_PROFILE_STREETADDRESS, ATTR_USER_PROFILE_CITY, ATTR_USER_PROFILE_STATE, ATTR_USER_PROFILE_ZIPCODE,
			ATTR_USER_PROFILE_COUNTRYCODE, ATTR_USER_PROFILE_POSTALADDRESS, ATTR_USER_PROFILE_PREFERREDLANGUAGE, ATTR_USER_PROFILE_LOCALE, ATTR_USER_PROFILE_TIMEZONE, ATTR_USER_PROFILE_USERTYPE, ATTR_USER_PROFILE_EMPLOYEENUMBER,
			ATTR_USER_PROFILE_COSTCENTER, ATTR_USER_PROFILE_ORGANIZATION, ATTR_USER_PROFILE_DIVISION, ATTR_USER_PROFILE_DEPARTMENT, ATTR_USER_PROFILE_MANAGERID, ATTR_USER_PROFILE_MANAGER,
			ATTR_USER_PROFILE_EMAIL,ATTR_USER_PROFILE_FIRSTNAME,ATTR_USER_PROFILE_LASTNAME,ATTR_USER_PROFILE_LOGIN,ATTR_USER_PROFILE_MOBILEPHONE,ATTR_USER_PROFILE_SECONDEMAIL
	};

	public static final String ATTR_USER_CREDENTIALS_PASSWORD = "passWord";
	public static final String ATTR_USER_CREDENTIALS_RECOVERY_QUESTION = "question";
	public static final String ATTR_USER_CREDENTIALS_RECOVERY_ANSWER = "answer";
	public static final String ATTR_USER_CREDENTIALS_PROVIDER_TYPE = "type";
	public static final String ATTR_USER_CREDENTIALS_PROVIDER_NAME = "name";
	public static final String ATTR_USER_CREDENTIALS_PROVIDER_TYPE_OKTA = "OKTA";
	public static final String ATTR_USER_CREDENTIALS_PROVIDER_NAME_OKTA = "OKTA";

}
