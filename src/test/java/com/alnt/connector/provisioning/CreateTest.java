/**
 * soori
 * 2019-11-21 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.access.provisioning.model.ISystemInformation;
import com.alnt.connector.constants.OktaConnectorConstants;
import com.alnt.connector.provisioning.services.OktaConnectionInterface;

/**
 * @author soori
 *
 */
public class CreateTest {
	private Map<String, String> connectionParams = null;
	Properties p = null;

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@Before
	public void setUp() throws Exception {
		p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put(OktaConnectorConstants.ORG_URL, p.getProperty(OktaConnectorConstants.ORG_URL));
		connectionParams.put(OktaConnectorConstants.ID_TOKEN, p.getProperty(OktaConnectorConstants.ID_TOKEN));
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * this user has no password , not enabled by default , user status will be "STAGED"
	 * 
	 * @throws Exception
	 */
	@Test
	public void createUserWithMandatoryDataAndWithoutPassw0rd() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		String _alertUserId = "CreateUserLogin@aler.de";
		//prov field mapping
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, _alertUserId);
		userParameters.put(OktaConnectorConstants.OKTA_USERID, _alertUserId);
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "CreateFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "CreateLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "CreateLN");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);// set status to DEPROVISIONED
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null); // delete record
	}

	/**
	 * this user has no password  and enabled , SO user status will be "PROVISIONED"
	 * 
	 * @throws Exception
	 */
	@Test
	public void createUserWithMandatoryDataAndWithoutPassw0rdAndActivate() throws Exception {
		connectionParams.put(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE, p.getProperty(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE));
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "CreateUserLogin@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "CreateFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "CreateLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "CreateLN");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null); // set status to DEPROVISIONED
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null); // delete record
	}
	
	/**
	 * this user has password , not enabled by default , user status will be "STAGED"
	 * 
	 * @throws Exception
	 */
	@Test
	public void createUserWithMandatoryDataAndWithPassw0rd() throws Exception {
		connectionParams.put(OktaConnectorConstants.SENSITIVE_ATTRIBUTES, OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD);
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "CreateUserLogin@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "CreateFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "CreateLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "CreateLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, "KsE2Exxxx1");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS); 
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null); // set status to DEPROVISIONED
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null); // delete record
	}

	/**
	 * this user has  password  and enabled , SO user status will be "ACTIVE"
	 * 
	 * @throws Exception
	 */
	@Test
	public void createUserWithMandatoryDataAndWithPassw0rdAndActivate() throws Exception {
		connectionParams.put(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE, p.getProperty(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE));
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "CreateUserLogin@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "CreateFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "CreateLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "CreateLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, "ksjdfh3sDsdhjsdf");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null); // set status to DEPROVISIONED
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null); // delete record
	}
	
	@Test
	public void createUserWithRecoveryQuestionAndActivate() throws Exception {
		connectionParams.put(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE, p.getProperty(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE));
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "CreateUserLogin@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "CreateFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "CreateLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "CreateLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, "ksjdfh3sDsdhjsdf");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_QUESTION, "What is your name");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_ANSWER, "asdsadd");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);// set status to DEPROVISIONED
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null); // delete record
	}
	

	@Test
	public void createUserWithInvalidProfileAttriute() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "CreateUserLogin@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "CreateFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "CreateLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "CreateLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, "ksjdfh3sDsdhjsdf");
		userParameters.put("InvalidKey", "Will throw Error");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
	}
	
	
	@Test
	public void createUserFullDataActive() throws Exception {
		connectionParams.put(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE, p.getProperty(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE));
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "CreateUserLogin@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "CreateFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "CreateLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "CreateLN");
		//optional -set 1 , required if need to create user as active
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, "ksjdfh3sDsdhjsdf");
		//optional -set 2
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_QUESTION, "What is your name");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_ANSWER, "asdsadd");
		//optional -set 3
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_SECONDEMAIL, "Crgin@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_MIDDLENAME, "ATTRR");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_HONORIFICPREFIX, "Mr");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_HONORIFICSUFFIX, "Dr");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_TITLE, "Mr");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_DISPLAYNAME, "Disp");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_NICKNAME, "Nick");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_PROFILEURL, "ProfUrl");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_PRIMARYPHONE, "1231231233");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_MOBILEPHONE, "1231231234");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_STREETADDRESS, "ST");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_CITY, "HYD");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_STATE, "TS");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_ZIPCODE, "500055");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_COUNTRYCODE, "IN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_POSTALADDRESS, "EDSSS");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_PREFERREDLANGUAGE, "fr");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOCALE, "te_IN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_TIMEZONE, "Asia/Kolkata");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_USERTYPE, "DDD");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMPLOYEENUMBER, "123123");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_COSTCENTER, "SSS");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_ORGANIZATION, "SDE");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_DIVISION, "DEV");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_DEPARTMENT, "RD");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_MANAGERID, "123123");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_MANAGER, "SDDDD");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		ISystemInformation isUserProvisionedResponse = connectionInterface.isUserProvisioned(userParameters.get(OktaConnectorConstants.OKTA_USERID).toString());
		assertTrue(isUserProvisionedResponse.isProvisioned());
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void createUserFullData() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "CreateUserLogin@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "CreateFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "CreateLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "CreateLN");
		//optional -set 1 , required if need to create user as active
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, "ksjdfh3sDsdhjsdf");
		//optional -set 2
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_QUESTION, "What is your name");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_RECOVERY_ANSWER, "asdsadd");
		//optional -set 3
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_SECONDEMAIL, "Crgin@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_MIDDLENAME, "ATTRR");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_HONORIFICPREFIX, "Mr");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_HONORIFICSUFFIX, "Dr");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_TITLE, "Mr");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_DISPLAYNAME, "Disp");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_NICKNAME, "Nick");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_PROFILEURL, "ProfUrl");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_PRIMARYPHONE, "1231231233");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_MOBILEPHONE, "1231231234");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_STREETADDRESS, "ST");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_CITY, "HYD");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_STATE, "TS");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_ZIPCODE, "500055");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_COUNTRYCODE, "IN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_POSTALADDRESS, "EDSSS");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_PREFERREDLANGUAGE, "fr");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOCALE, "te_IN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_TIMEZONE, "Asia/Kolkata");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_USERTYPE, "DDD");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMPLOYEENUMBER, "123123");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_COSTCENTER, "SSS");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_ORGANIZATION, "SDE");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_DIVISION, "DEV");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_DEPARTMENT, "RD");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_MANAGERID, "123123");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_MANAGER, "SDDDD");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		ISystemInformation isUserProvisionedResponse = connectionInterface.isUserProvisioned(userParameters.get(OktaConnectorConstants.OKTA_USERID).toString());
		assertTrue(isUserProvisionedResponse.isProvisioned());
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void checkIsUserProvisionedRandonUUID() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		ISystemInformation isUserProvisionedResponse = connectionInterface.isUserProvisioned(UUID.randomUUID().toString());
		assertFalse(isUserProvisionedResponse.isProvisioned());
	}

	@Test
	public void checkIsUserProvisionedInvalidUUID() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		ISystemInformation isUserProvisionedResponse = connectionInterface.isUserProvisioned(UUID.randomUUID().toString().concat("-Extra"));
		assertFalse(isUserProvisionedResponse.isProvisioned());
	}
}
