/**
 * soori
 * 2019-11-21 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertTrue;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.connector.constants.OktaConnectorConstants;
import com.alnt.connector.provisioning.services.OktaConnectionInterface;
import com.alnt.extractionconnector.common.constants.IExtractionConstants;
import com.alnt.extractionconnector.common.service.ISearchCallback;
import com.alnt.extractionconnector.user.model.ExtractorAttributes;

/**
 * @author soori
 *
 */
public class ReconUsersTest {
	private Map<String, String> connectionParams = null;
	Map<String, List<ExtractorAttributes>> options = new HashMap<String, List<ExtractorAttributes>>();

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put(OktaConnectorConstants.ORG_URL, p.getProperty(OktaConnectorConstants.ORG_URL));
		connectionParams.put(OktaConnectorConstants.ID_TOKEN, p.getProperty(OktaConnectorConstants.ID_TOKEN));
		connectionParams.put(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE, p.getProperty(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE));
		
		
		List<ExtractorAttributes> attributes = null;
		ExtractorAttributes userAttribute = null;
		
		
		//Extract Okta User Id to  Master User Id , employee Id 
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes("MasterUserId", "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		userAttribute = new ExtractorAttributes("EmployeeId", "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		userAttribute = new ExtractorAttributes(OktaConnectorConstants.ALERT_USERID, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(OktaConnectorConstants.OKTA_USERID, attributes);
		
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes("Email", "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, attributes);
		
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes("First Name", "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, attributes);
		
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes("COST Center", "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(OktaConnectorConstants.ATTR_USER_PROFILE_COSTCENTER, attributes);
		
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes("XYZ", "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setRoleAttr(true);
		attributes.add(userAttribute);
		
		//Extract Okta role ID to  Alert Display role name 
		userAttribute = new ExtractorAttributes("TechnicalRoleName", "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setRoleAttr(true);
		attributes.add(userAttribute);
		options.put(OktaConnectorConstants.ATTR_ROLE_ID, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes("DisplayRoleName", "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setRoleAttr(true);
		attributes.add(userAttribute);
		options.put(OktaConnectorConstants.ATTR_ROLE_NAME, attributes);
		userAttribute = new ExtractorAttributes("Role Description", "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setRoleAttr(true);
		attributes.add(userAttribute);
		options.put(OktaConnectorConstants.ATTR_ROLE_DESC, attributes);
		
		
		
		
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void userReconTest() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		ISearchCallback callback = new SearchCallback();
		connectionInterface.getAllUsersWithCallback(options, 10, null, callback);
		assertTrue(true);
	}
	
	@Test
	public void userReconTestToCombineFaciltyCodeandBadgeId() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		ISearchCallback callback = new SearchCallback();
		connectionInterface.getAllUsersWithCallback(options, 10, null, callback);
		assertTrue(true);
	}
	
	@Test
	public void userIncrementalReconTest() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		ISearchCallback callback = new SearchCallback();
		connectionInterface.getIncrementalUsersWithCallback(new Date(), options, 0, null, callback);
		assertTrue(true);
	}
	
	@Test
	public void userIncrementalMonthReconTest() throws Exception {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		ISearchCallback callback = new SearchCallback();
		connectionInterface.getIncrementalUsersWithCallback(cal.getTime(), options, 0, null, callback);
		assertTrue(true);
	}

	@Test
	public void userReconTestInvalidEndpoint() throws Exception {
		connectionParams.put("baseURL", "https://rs2tech.com:7777/v2.0/");
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		ISearchCallback callback = new SearchCallback();
		Map<String, List<ExtractorAttributes>> options = new HashMap<String, List<ExtractorAttributes>>();
		connectionInterface.getAllUsersWithCallback(options, 10, null, callback);
		assertTrue(true);

	}

}
