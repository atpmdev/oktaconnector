package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.OktaConnectorConstants;
import com.alnt.connector.provisioning.model.Group;
import com.alnt.connector.provisioning.model.RoleInformation;
import com.alnt.connector.provisioning.services.OktaConnectionInterface;
import com.alnt.extractionconnector.common.constants.IExtractionConstants;
import com.alnt.extractionconnector.user.model.ExtractorAttributes;
import com.alnt.fabric.component.rolemanagement.search.IRoleInformation;

/**
 * 
 * @author soori
 *
 */
public class ChangeAccessTest {
	private Map<String, String> connectionParams = null;
	public String _baseUrl = null;
	public String _apiUserName = null;
	public String _apiPassword = null;
	public String _publicKey = null;
	public String _alertAppDateFormat = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * soori 2019-11-27 - 4:30:49 pm void
	 * 
	 * @throws Exception
	 *
	 */

	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put(OktaConnectorConstants.ORG_URL, p.getProperty(OktaConnectorConstants.ORG_URL));
		connectionParams.put(OktaConnectorConstants.ID_TOKEN, p.getProperty(OktaConnectorConstants.ID_TOKEN));
		connectionParams.put(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE, p.getProperty(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE));
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * if both provisioning roles & deprovisioning roles are null/ empty should it
	 * return success or failure.
	 */

	@Test
	public void changeAccessWithoutRolesToChange() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		IProvisioningResult response = connectionInterface.changeAccess(null, null, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void changeAccessForWithOutUserParam() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		List provisionRoles = new ArrayList();
		provisionRoles.add(prepareRoleInformation("role1", "desc", null, null));
		IProvisioningResult response = connectionInterface.changeAccess(null, provisionRoles, userParameters, null,
				null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_FAILURE);
		assertEquals(response.getMsgDesc(), "UserId does not passed!!");
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void changeAccessForWithNullUserParam() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters =null;
		List provisionRoles = new ArrayList();
		provisionRoles.add(prepareRoleInformation("role1", "desc", null, null));
		IProvisioningResult response = connectionInterface.changeAccess(null, provisionRoles, userParameters, null,
				null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_FAILURE);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void changeAccessForWithRandomUserId() throws Exception {
		connectionParams.put(OktaConnectorConstants.SHOW_PROVISION_WARNINGS, "False");
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.OKTA_USERID, UUID.randomUUID().toString());
		List provisionRoles = new ArrayList();
		provisionRoles.add(prepareRoleInformation("role1", "desc", null, null));
		IProvisioningResult response = connectionInterface.changeAccess(null, provisionRoles, userParameters, null,
				null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_FAILURE);
		assertEquals(response.getMsgDesc(), "User does not exist in external system");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void changeAccessForWithRandomUserIdbutNoWarningsFlag() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.OKTA_USERID, UUID.randomUUID().toString());
		List provisionRoles = new ArrayList();
		provisionRoles.add(prepareRoleInformation("role1", "desc", null, null));
		IProvisioningResult response = connectionInterface.changeAccess(null, provisionRoles, userParameters, null,
				null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
	}

	@Test
	public void changeAccessAddValidRole() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "ChangeAccessUserLogin@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "ChangeAccessFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "ChangeAccessLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "ChangeAccessLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, "Passw0rd");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		List<IRoleInformation> roles = convertAccessLevelsToRoleInfo(connectionInterface.getAllGroups());
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	

	@Test
	public void changeAccessDeleteValidRoleWithBothDates() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "ChangeAccessUserLogin@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "ChangeAccessFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "ChangeAccessLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "ChangeAccessLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, "Passw0rd");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		List<IRoleInformation> roles = convertAccessLevelsToRoleInfo(connectionInterface.getAllGroups());
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
		List<IRoleInformation> deProvisionRoles = new ArrayList<IRoleInformation>();
		for(IRoleInformation  addedROle : roles ) {
			if(!addedROle.getDescription().equalsIgnoreCase("Everyone")) {
				deProvisionRoles.add(addedROle);
			}
		}
		response = connectionInterface.changeAccess(null, null, userParameters, null, deProvisionRoles, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	private IRoleInformation prepareRoleInformation(String name, String desc, String ValidFrom, String ValidTo) {

		IRoleInformation roleInformation = new RoleInformation();
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(name);
		roleInformation.setDescription(desc);
		return roleInformation;
	}

	private List<IRoleInformation> convertAccessLevelsToRoleInfo(List<Group> groups) {
		List<IRoleInformation> rolesList = new ArrayList<IRoleInformation>();
		for (Group group : groups) {
			String roleId = group.getId();
			String roleName = group.getProfile().getName();
			// String roleDesc = group.getProfile().getDescription();
			Map<String, List<ExtractorAttributes>> options = new HashMap<String, List<ExtractorAttributes>>();
			List<ExtractorAttributes> attributes = new ArrayList<ExtractorAttributes>();
			ExtractorAttributes roleAttribute = new ExtractorAttributes(OktaConnectorConstants.ALERT_ROLE_NAME, "",
					IExtractionConstants.TYPE.STRING);
			roleAttribute.setRoleAttr(true);
			attributes.add(roleAttribute);
			options.put(OktaConnectorConstants.ATTR_ROLE_ID, attributes);
			IRoleInformation roleInformation = getRoleInformation(options, roleId, roleName);
			Map<String, List<String>> memberData = new HashMap<String, List<String>>();
			List<String> values = new ArrayList<String>();
			values.add("" + roleId);
			memberData.put(IExtractionConstants.TECHNICAL_ROLE_NAME, values);
			getRoleAttribute(options, "", "", memberData);
			roleInformation.setMemberData(memberData);
			rolesList.add(roleInformation);
		}
		return rolesList;
	}

	private IRoleInformation getRoleInformation(Map<String, List<ExtractorAttributes>> options, String roleId,
			String roleDesc) {
		IRoleInformation roleInformation = new RoleInformation();
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(roleDesc);
		List<ExtractorAttributes> aeRoleNameAttr = options.get(OktaConnectorConstants.ATTR_ROLE_ID);
		if (aeRoleNameAttr != null) {
			for (ExtractorAttributes extractorAttributes : aeRoleNameAttr) {
				if (extractorAttributes.isRoleAttr() && OktaConnectorConstants.ALERT_ROLE_NAME.equals(extractorAttributes.getAttributeName())) {
					roleInformation.setName(roleId);
				}
			}
		}
		roleInformation.setDescription(roleDesc);
		return roleInformation;
	}

	private void getRoleAttribute(Map<String, List<ExtractorAttributes>> options, String attributeName,
			String attributeValue, Map<String, List<String>> memberData) {
		if (options.containsKey(attributeName)) {
			List<String> values = new ArrayList<String>();
			List<ExtractorAttributes> companyAttr = options.get(attributeName);
			for (ExtractorAttributes extractorAttributes : companyAttr) {
				if (extractorAttributes.isRoleAttr()) {
					values = new ArrayList<String>();
					values.add("" + attributeValue);
					memberData.put(extractorAttributes.getAttributeName(), values);
				}
			}
		}
	}

}
