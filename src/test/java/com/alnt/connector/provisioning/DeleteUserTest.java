/**
 * soori
 * 2019-11-21 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.OktaConnectorConstants;
import com.alnt.connector.provisioning.services.OktaConnectionInterface;

/**
 * @author soori
 *
 */
public class DeleteUserTest {
	private Map<String, String> connectionParams = null;

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put(OktaConnectorConstants.ORG_URL, p.getProperty(OktaConnectorConstants.ORG_URL));
		connectionParams.put(OktaConnectorConstants.ID_TOKEN, p.getProperty(OktaConnectorConstants.ID_TOKEN));
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void deleteWithoutCardHolderIdParam() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		IProvisioningResult response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_FAILURE);
	}

	@Test
	public void deleteWithoutCardHolderIdNullParam() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = null;
		IProvisioningResult response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_FAILURE);
	}

	@Test
	public void deleteRandomCardHolderIdParam() throws Exception {
		connectionParams.put(OktaConnectorConstants.SHOW_PROVISION_WARNINGS, "False");
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.OKTA_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_FAILURE);
	}

	@Test
	public void deleteRandomCardHolderId() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.OKTA_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_SUCCESS);
	}

	@Test
	public void deleteUserProvisioned() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "DeleteUserLogin@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "DeleteFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "DeleteLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "DeleteLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, "ksjdfh3sDsdhjsdf");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_SUCCESS);
	}

	@Test
	public void deleteAlreadyDeletedUserwithoutConnParamDefaultTrue() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "DeleteUserLogin@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "DeleteFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "DeleteLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "DeleteLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, "ksjdfh3sDsdhjsdf");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_SUCCESS);
	}

	@Test
	public void deleteAlreadyDeletedUserConnParamShowWarningsFalse() throws Exception {
		connectionParams.put(OktaConnectorConstants.SHOW_PROVISION_WARNINGS, "True");
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "DeleteUserLogin@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "DeleteFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "DeleteLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "DeleteLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, "ksjdfh3sDsdhjsdf");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_SUCCESS);
	}

	@Test
	public void deleteAlreadyDeletedUserConnParamShowWarningsTrue() throws Exception {
		connectionParams.put(OktaConnectorConstants.SHOW_PROVISION_WARNINGS, "False");
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "DeleteUserLogin@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "DeleteFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "DeleteLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "DeleteLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, "ksjdfh3sDsdhjsdf");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_FAILURE);
	}

}
