package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.OktaConnectorConstants;
import com.alnt.connector.provisioning.services.OktaConnectionInterface;

/**
 * 
 * @author soori
 *
 */
public class UnLockTest {
	private Map<String, String> connectionParams = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * soori 2019-11-27 - 4:30:49 pm void
	 * 
	 * @throws Exception
	 *
	 */

	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put(OktaConnectorConstants.ORG_URL, p.getProperty(OktaConnectorConstants.ORG_URL));
		connectionParams.put(OktaConnectorConstants.ID_TOKEN, p.getProperty(OktaConnectorConstants.ID_TOKEN));
		connectionParams.put(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE, p.getProperty(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void unLockUserWithoutCardHolderIdParam() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		IProvisioningResult response = connectionInterface.unlock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.UNLOCK_USER_FAILURE);
	}
	
	@Test
	public void unLockUserWithNullParam() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = null;
		IProvisioningResult response = connectionInterface.unlock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.UNLOCK_USER_FAILURE);
	}

	@Test
	public void unLockUserRandomCardHolderIdParam() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.OKTA_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.unlock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.UNLOCK_USER_FAILURE);
	}

	@Test
	public void unLockUserUserProvisioned() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "UnLockUser@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "UnLockFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "UnLockLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "UnLockLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, "PassW0rd");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		response = connectionInterface.unlock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.UNLOCK_USER_SUCCESS);
		assertFalse(connectionInterface.isUserLocked(userParameters.get(OktaConnectorConstants.OKTA_USERID).toString()));
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void unLockAlreadyDeletedUserConnParamShowWarningsTrue() throws Exception {
		connectionParams.put(OktaConnectorConstants.SHOW_PROVISION_WARNINGS, "False");
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "UnLockUser@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "UnLockFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "UnLockLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "UnLockLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, "PassW0rd");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		response = connectionInterface.unlock(2321L, null, userParameters, null, null);
		response = connectionInterface.unlock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.UNLOCK_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void unLockAlreadyDeletedUserwithoutConnParamDefaultTrue() throws Exception {
		connectionParams.put(OktaConnectorConstants.SHOW_PROVISION_WARNINGS, "True");
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "UnLockUser@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "UnLockFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "UnLockLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "UnLockLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, "PassW0rd");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		response = connectionInterface.unlock(2321L, null, userParameters, null, null);
		response = connectionInterface.unlock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.UNLOCK_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
}
