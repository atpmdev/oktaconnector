/**
 * 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.connector.constants.OktaConnectorConstants;
import com.alnt.connector.exception.OktaConnectorException;
import com.alnt.connector.provisioning.services.OktaConnectionInterface;

/**
 * @author soori
 *
 */
public class TestConnectionTest {
	private Map<String, String> connectionParams = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put(OktaConnectorConstants.ORG_URL, p.getProperty(OktaConnectorConstants.ORG_URL));
		connectionParams.put(OktaConnectorConstants.ID_TOKEN, p.getProperty(OktaConnectorConstants.ID_TOKEN));
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = OktaConnectorException.class)
	public void nullConnectionParams() throws Exception {
		new OktaConnectionInterface(null);
	}

	@Test
	public void validCredentials() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		boolean testPassed = connectionInterface.testConnection();
		assertTrue(testPassed);
	}

	@Test
	public void wrongIdToken() throws Exception {
		connectionParams.put(OktaConnectorConstants.ID_TOKEN, "131231231231");
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	}

	@Test
	public void wrongEndpoint() throws Exception {
		connectionParams.put(OktaConnectorConstants.ORG_URL, "https://lawa-imcs-dev-admin.okta.com");
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	}

	@Test
	public void emptyEndpoint() throws Exception {
		connectionParams.put(OktaConnectorConstants.ORG_URL, "");
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	}

	// TODO :: ideally this should fail , connector should have check for mandatory fields
	// too many test cases are skipped ( missing parameter , has parameter but value is null )
	@Test
	public void emptyMap() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(new HashMap<String, String>());
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	}

}
