package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.OktaConnectorConstants;
import com.alnt.connector.provisioning.services.OktaConnectionInterface;

/**
 * 
 * @author soori
 *
 */
public class UpdateTest {
	private Map<String, String> connectionParams = null;
	Properties p = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * soori 2019-11-27 - 4:30:49 pm void
	 * 
	 * @throws Exception
	 *
	 */

	@Before
	public void setUp() throws Exception {
		p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put(OktaConnectorConstants.ORG_URL, p.getProperty(OktaConnectorConstants.ORG_URL));
		connectionParams.put(OktaConnectorConstants.ID_TOKEN, p.getProperty(OktaConnectorConstants.ID_TOKEN));
		connectionParams.put(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE, p.getProperty(OktaConnectorConstants.ACTIVATE_USER_WHILE_CREATE));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void updateUserWithoutCardHolderIdParam() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		IProvisioningResult response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
	}

	@Test
	public void updateUserWitNullParam() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = null;
		IProvisioningResult response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
	}

	@Test
	public void updateUserRandomCardHolderIdParam() throws Exception {
		connectionParams.put(OktaConnectorConstants.SHOW_PROVISION_WARNINGS, "False");
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.OKTA_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
	}

	@Test
	public void updateUserRandomCardHolderIdAndIgnoreErrorsParam() throws Exception {
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.OKTA_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
	}

	@Test
	public void updateUserUserProvisioned() throws Exception {
		connectionParams.put(OktaConnectorConstants.SENSITIVE_ATTRIBUTES, p.getProperty(OktaConnectorConstants.SENSITIVE_ATTRIBUTES));
		connectionParams.put(OktaConnectorConstants.CUSTOM_ATTRIBUTES, p.getProperty(OktaConnectorConstants.CUSTOM_ATTRIBUTES));
		OktaConnectionInterface connectionInterface = new OktaConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "UpdateUser@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "UpdateFN@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "UpdateLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "UpdateLN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_CREDENTIALS_PASSWORD, "PassW0rd");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_MOBILEPHONE, "1231232123");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_SECONDEMAIL, "second@sadsa.sdf");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_CITY, "HYD");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_STATE, "TS");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_ZIPCODE, "500055");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOGIN, "UpdateUserupdated@aler.de");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMAIL, "UpdateFNupdated@demo.com");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_FIRSTNAME, "UpdateLNupdated");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LASTNAME, "UpdateLNupdated");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_MOBILEPHONE, "1231232123111");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_SECONDEMAIL, "secondupdated@sadsa.sdf");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_STREETADDRESS, "ST");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_CITY, "HYD_UPDATED");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_STATE, "TS_UPDATED");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_ZIPCODE, "110001");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_COUNTRYCODE, "IN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_POSTALADDRESS, "EDSSS");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_PREFERREDLANGUAGE, "fr");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_LOCALE, "te_IN");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_TIMEZONE, "Asia/Kolkata");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_USERTYPE, "DDD");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_EMPLOYEENUMBER, "123123");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_COSTCENTER, "SSS");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_ORGANIZATION, "SDE");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_DIVISION, "DEV");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_DEPARTMENT, "RD");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_MANAGERID, "123123");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_MANAGER, "SDDDD");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_TITLE, "Title");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_DISPLAYNAME, "DisplayName");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_NICKNAME, "NickName");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_PROFILEURL, "profle Url");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_PRIMARYPHONE, "12312312321");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_MIDDLENAME, "middleName");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_HONORIFICPREFIX, "honorificPrefix");
		userParameters.put(OktaConnectorConstants.ATTR_USER_PROFILE_HONORIFICSUFFIX, "honorificSuffix");
		// custom Attributes
		userParameters.put("custom", "custom");
		userParameters.put("custom2", "custom2");
		response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

}
